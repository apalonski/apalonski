package com.asoi.palonskiy;

import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;


public class LibraryTest {
    private Library library;
    private ByteArrayOutputStream outContent = new ByteArrayOutputStream();

    @Before
    public void initialize() {
        List<Book> list = new ArrayList<>();
        list.add(new Book("Book1", "date1", "author1", "publisher1"));
        list.add(new Book("Book2", "date2", "author2", "publisher2"));
        list.add(new Book("Book3", "date3", "author3", "publisher3"));
        list.add(new Book("Book4", "date4", "author4", "publisher4"));
        library = new Library(list);
        System.setOut(new PrintStream(outContent));
    }

    @Test
    public void searchTest() {
        String line = "Book1 date1 by author1 published by publisher1";
        library.search("Book1");
        assertEquals(line, outContent.toString());
    }

    @Test
    public void searchByAuthor() {
        String line = "Book1 date1 by author1 published by publisher1";
        library.searchByAuthor("AuThOr1");
        assertEquals(line, outContent.toString());
    }

    @Test
    public void searchTest_ExpectedErrorMessage() {
        String line = "The book wasn't found\n";
        library.search("Book6");
        assertEquals(line, outContent.toString());
    }

    @Test
    public void searchByAuthor_ExpectedErrorMessage() {
        String line = "The book wasn't found\n";
        library.searchByAuthor("AuThOr6");
        assertEquals(line, outContent.toString());
    }
}