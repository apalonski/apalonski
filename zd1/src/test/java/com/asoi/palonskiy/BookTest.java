package com.asoi.palonskiy;

import org.junit.Before;
import org.junit.Test;


import static org.junit.Assert.*;

public class BookTest {
    private Book book;

    @Before
    public void initialize() {
        book = new Book("Book1", "date1", "author1", "publisher1");
    }

    @Test
    public void getAuthorTest() {
        assertEquals(book.getAuthor(), "author1");
    }

    @Test
    public void getDateTest() {
        assertEquals(book.getDate(), "date1");
    }

    @Test
    public void getNameTest() {
        assertEquals(book.getName(), "Book1");
    }

    @Test
    public void getPublisherTest() {
        assertEquals(book.getPublisher(), "publisher1");
    }

    @Test
    public void printTest() {
        String line = "Book1 date1 by author1 published by publisher1";
        assertEquals(line, book.toString());
    }
}