package com.asoi.palonskiy.zd2;

public class Book {
    private String name;
    private String date;
    private String author;
    private String publisher;

    public Book(String name, String date, String author, String publisher) {
        this.author = author;
        this.name = name;
        this.date = date;
        this.publisher = publisher;
    }

    public String getAuthor() {
        return author;
    }

    public String getDate() {
        return date;
    }

    public String getName() {
        return name;
    }

    public String getPublisher() {
        return publisher;
    }


    public String toString() {
        return String.format("%s %s by %s published by %s", name, date, author, publisher);
    }
}
