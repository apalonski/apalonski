package com.asoi.palonskiy.zd2;

import java.util.ArrayList;
import java.util.List;

public class Client {
    public static void main(String[] args) {
        List<Book> list = new ArrayList<>();
        list.add(new Book("Book1", "date1", "author1", "publisher1"));
        list.add(new Book("Book2", "date2", "author2", "publisher2"));
        list.add(new Book("Book3", "date3", "author3", "publisher3"));
        list.add(new Book("Book4", "date4", "author4", "publisher4"));
        Library library = new Library(list);
        library.searchByAuthor("b");
        library.search("Book1");
    }
}
