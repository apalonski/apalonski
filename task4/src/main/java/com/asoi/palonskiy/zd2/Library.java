package com.asoi.palonskiy.zd2;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class Library {
    private List<Book> list;
    private static final Logger logger = LoggerFactory.getLogger(Library.class);

    public Library(List<Book> list) {
        this.list = list;
    }

    public void search(String name) {
        list.stream()
                .filter(book -> book.getName().equals(name))
                .forEach(book -> logger.info(book.toString()));
        checkName(name);
    }

    public void searchByAuthor(String name) {
        list.stream()
                .filter(book -> book.getAuthor().toLowerCase().contains(name.toLowerCase()))
                .forEach(book -> logger.info(book.toString()));
        checkAuthor(name);
    }

    public void checkName(String name) {
        boolean flag = list.stream()
                .anyMatch(book -> book.getName().equals(name));
        if (!flag) {
            logger.error("The book wasn't found\n");
        }
    }
    public void checkAuthor(String author) {
        boolean flag = list.stream()
                .anyMatch(book -> book.getAuthor().toLowerCase().contains(author.toLowerCase()));
        if (!flag) {
            logger.error("The book wasn't found\n");
        }
    }
}
