package com.asoi.palonskiy.service;

import com.asoi.palonskiy.model.Book;

import java.util.List;

public interface BookService {
     List<Book> search(String name);
     List<Book> searchByAuthor(String name);
}
