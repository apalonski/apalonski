package com.asoi.palonskiy.controllers;


import com.asoi.palonskiy.model.Book;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class MainServlet extends HttpServlet {
    private static List<Book> list = new ArrayList<>();

    public MainServlet() {
        list.add(new Book(1, "Book1", "date1", "author1", "publisher1"));
        list.add(new Book(2, "Book2", "date2", "author2", "publisher2"));
        list.add(new Book(3, "Book3", "date3", "author3", "publisher3"));
        list.add(new Book(4, "Book4", "date4", "author4", "publisher4"));
    }


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();
        session.setAttribute("books", list);
        try {
            if (req.getParameter("ListOfBooks") != null) {
                getServletContext().getRequestDispatcher("/list.jsp").forward(req, resp);
            } else if (req.getParameter("Search") != null) {
                getServletContext().getRequestDispatcher("/search.jsp").forward(req, resp);
            } else if (req.getParameter("Update/Add") != null) {
                if (req.getParameter("id").equals("new")) {
                    getServletContext().getRequestDispatcher("/add.jsp").forward(req, resp);
                } else if (Integer.parseInt(req.getParameter("id")) != 0 && (Integer.parseInt(req.getParameter("id")) <= list.size())) {
                    session.setAttribute("id", req.getParameter("id"));
                    getServletContext().getRequestDispatcher("/update.jsp").forward(req, resp);
                } else throw new NumberFormatException();

            }
        } catch (NumberFormatException ex) {
            getServletContext().getRequestDispatcher("/error.jsp").forward(req, resp);
        }

    }
}
