package com.asoi.palonskiy.controllers;

import com.asoi.palonskiy.model.Book;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class UpdateAddServlet extends HttpServlet {
    private static final String BOOKS = "books";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();
        List<Book> list = (ArrayList<Book>) session.getAttribute(BOOKS);
        long id = (long)(list.size() + 1);
        listFormer(id, req, resp);

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();
        long id = Long.parseLong(session.getAttribute("id").toString());
        listFormer(id, req, resp);
    }

    private void listFormer(long id, HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();
        List<Book> list = (ArrayList<Book>) session.getAttribute(BOOKS);
        Book book = new Book(id, req.getParameter("name"), req.getParameter("date"), req.getParameter("author"), req.getParameter("publisher"));
        if (id > list.size()) {
            list.add(book);
        } else {
            list.set((int) id - 1, book);
        }
        session.setAttribute(BOOKS, list);
        req.getRequestDispatcher("/list.jsp").forward(req, resp);
    }

}
