package com.asoi161.palonskiy.system;

import com.asoi161.palonskiy.exception.ValidationFaildExceprion;
import org.junit.Test;

import static org.junit.Assert.*;

public class ValidationSystemTest {

    @Test
    public void testValidateInt () throws ValidationFaildExceprion {
        ValidationSystem.validate(1);
        ValidationSystem.validate(5);
        ValidationSystem.validate(10);

    }

    @Test (expected = ValidationFaildExceprion.class)
    public void testValidateIntFails() throws ValidationFaildExceprion {
        ValidationSystem.validate(11);
    }

    @Test (expected = ValidationFaildExceprion.class)
    public void testValidateIntFails2 () throws ValidationFaildExceprion {
        ValidationSystem.validate(0);
    }

    @Test
    public void testValidateString () throws ValidationFaildExceprion {
        ValidationSystem.validate("Hello");
        ValidationSystem.validate("Hello world, abc");
    }

    @Test (expected = ValidationFaildExceprion.class)
    public void testValidateStringFails() throws ValidationFaildExceprion {
        ValidationSystem.validate("hello");
    }

    @Test (expected = ValidationFaildExceprion.class)
    public void testValidateStringFails2() throws ValidationFaildExceprion {
        ValidationSystem.validate("");
    }
    @Test (expected = ValidationFaildExceprion.class)
    public void testValidateUnknownTypeFails() throws ValidationFaildExceprion {
        ValidationSystem.validate('g');
    }
}