package com.asoi161.palonskiy.system;

import com.asoi161.palonskiy.exception.ValidationFaildExceprion;
import com.asoi161.palonskiy.validators.IntegerValidator;
import com.asoi161.palonskiy.validators.StringValidator;

import java.util.ArrayList;
import java.util.List;

public final class ValidationSystem {

    private static List<Validator> listOfValidators = new ArrayList<>();

    private ValidationSystem() {

    }

    private static <T> Validator сhooseRightValidator(T object) throws ValidationFaildExceprion {
        listOfValidators.add(new IntegerValidator());
        listOfValidators.add(new StringValidator());
        boolean flag;
        int intFlag = 10;
        for (int i = 0; i < listOfValidators.size(); i++) {
            flag = object.getClass().toString().equals(listOfValidators.get(i).getMyClass());
            if (flag) {
                intFlag = i;
            }
        }
        if (intFlag != 10) {
            return listOfValidators.get(intFlag);
        } else {
            throw new ValidationFaildExceprion("Wrong type");
        }
    }

    public static <T> void validate(T object) throws ValidationFaildExceprion {
        Validator rightValidator;
        rightValidator = сhooseRightValidator(object);
        rightValidator.validate(object);
    }
}
