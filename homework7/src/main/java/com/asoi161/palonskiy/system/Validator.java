package com.asoi161.palonskiy.system;

public interface Validator<T> {
    void validate(T object);

    String getMyClass();
}

