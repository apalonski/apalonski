package com.asoi161.palonskiy.validators;

import com.asoi161.palonskiy.exception.ValidationFaildExceprion;
import com.asoi161.palonskiy.system.Validator;

public class IntegerValidator implements Validator<Integer> {

    public void validate(Integer number) {
        if (number <= 10 && number >= 1 && number != null) {
            System.out.print("successful Integer validation");
        } else {
            throw new ValidationFaildExceprion("Integer validation faild");
        }
    }

    @Override
    public String getMyClass() {
        return Integer.class.toString();
    }
}

