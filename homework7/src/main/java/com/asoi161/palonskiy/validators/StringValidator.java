package com.asoi161.palonskiy.validators;

import com.asoi161.palonskiy.exception.ValidationFaildExceprion;
import com.asoi161.palonskiy.system.Validator;

public class StringValidator implements Validator<String> {

    public void validate(String line) {
        if (!line.equals("")) {
            if (line.matches("^[A-Z]+.*")) {
                System.out.print("successful String validation");
            } else {
                throw new ValidationFaildExceprion("String validation faild");
            }
        } else {
            throw new ValidationFaildExceprion("String validation faild");
        }
    }

    @Override
    public String getMyClass() {
        return String.class.toString();
    }
}
