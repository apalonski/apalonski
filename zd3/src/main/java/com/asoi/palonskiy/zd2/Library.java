package com.asoi.palonskiy;

import java.util.List;

public class Library {
    private List<Book> list;

    public Library(List<Book> list) {
        this.list = list;
    }

    public void search(String name) {
        list.stream()
                .filter(book -> book.getName().equals(name))
                .forEach(book -> System.out.print(book));
        checkName(name);
    }

    public void searchByAuthor(String name) {
        list.stream()
                .filter(book -> book.getAuthor().toLowerCase().contains(name.toLowerCase()))
                .forEach(book -> System.out.print(book));
        checkAuthor(name);
    }

    public void checkName(String name) {
        boolean flag = list.stream()
                .anyMatch(book -> book.getName().equals(name));
        if (!flag) {
            System.out.print("The book wasn't found\n");
        }
    }
    public void checkAuthor(String author) {
        boolean flag = list.stream()
                .anyMatch(book -> book.getAuthor().toLowerCase().contains(author.toLowerCase()));
        if (!flag) {
            System.out.print("The book wasn't found\n");
        }
    }
}
