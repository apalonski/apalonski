package com.asoi.palonskiy;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class Servlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp){
        try {
            PrintWriter out = resp.getWriter();
            out.print("<h1>Hello from App developed by Sasha</h1>");
        }
        catch (IOException ex){
            System.out.print(ex.getMessage());
        }

    }
}