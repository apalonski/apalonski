package com.asoi.palonskiy.service;

import com.asoi.palonskiy.dao.UsersDao;
import com.asoi.palonskiy.dao.UsersDaoImpl;
import com.asoi.palonskiy.model.User;


import java.util.List;

public class UserServiceImpl implements UserService {

    public UserServiceImpl() {

    }


    public boolean checkUser(String name) {
        UsersDao usersDao = new UsersDaoImpl();
        List<User> users = usersDao.getUser();
        return users.stream()
                .anyMatch(user -> user.getName().equals(name));

    }

    public boolean checkPassword(String name) {
        UsersDao usersDao = new UsersDaoImpl();
        List<User> users = usersDao.getUser();
        return users.stream()
                .anyMatch(user -> user.getPassword().equals(name));
    }
}
