package com.asoi.palonskiy.controllers;

import com.asoi.palonskiy.model.Book;
import com.asoi.palonskiy.service.BookService;
import com.asoi.palonskiy.service.BookServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/books/search")
public class SearchServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String name = req.getParameter("name");
        BookService bookServiceImpl = new BookServiceImpl();
        List<Book> correct = bookServiceImpl.search(name);
        req.setAttribute("correct", correct);
        check(correct, req);
        req.getRequestDispatcher("/searchResult.jsp").forward(req, resp);
    }

    public void check(List<Book> list, HttpServletRequest req) {
        if (list.isEmpty()) {
            req.setAttribute("correct", "The book wasn't found.");
        }
    }
}
