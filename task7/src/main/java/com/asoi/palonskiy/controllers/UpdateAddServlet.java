package com.asoi.palonskiy.controllers;

import com.asoi.palonskiy.model.Book;
import com.asoi.palonskiy.service.BookService;
import com.asoi.palonskiy.service.BookServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

@WebServlet("/books/updateAdd")
public class UpdateAddServlet extends HttpServlet {
    private static final String BOOKS = "books";
    private final BookService bookService;

    public UpdateAddServlet() {
        bookService = new BookServiceImpl();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        long id = (long) bookService.getList().size() + 1;
        bookService.insert(id, req);
        List<Book> list = bookService.getList();
        HttpSession session = req.getSession();
        session.setAttribute(BOOKS, list);
        req.getRequestDispatcher("/list.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();
        long id = Long.parseLong((session.getAttribute("id")).toString());
        bookService.update(id, req);
        List<Book> list = bookService.getList();
        session.setAttribute(BOOKS, list);
        req.getRequestDispatcher("/list.jsp").forward(req, resp);
    }


}
