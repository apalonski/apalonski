package com.asoi.palonskiy.dao;

import com.asoi.palonskiy.model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class UsersDaoImpl implements UsersDao {
    private static final String DB_URL = "jdbc:h2:mem:test;DB_CLOSE_DELAY=-1";
    private static final String USER = "sa";
    private static final String PASS = "";
    private static final Logger LOGGER = LoggerFactory.getLogger(BookDaoImpl.class);

    public List<User> getUser(){
        List<User> users = new ArrayList<>();
        String sql = "SELECT login, password FROM USERS";
        try (Connection conn = DriverManager.getConnection(DB_URL, USER, PASS); Statement stmt = conn.createStatement(); ResultSet rs = stmt.executeQuery(sql);) {
            while (rs.next()) {
                String user = rs.getString("login");
                String password = rs.getString("password");
                users.add(new User(user,password));
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }
        return users;
    }

}
