package com.asoi.palonskiy.filter;


import com.asoi.palonskiy.service.UserService;
import com.asoi.palonskiy.service.UserServiceImpl;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebFilter(urlPatterns = {"/filterServlet", "/books/*"})
public class UserFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        UserService user = new UserServiceImpl();
        HttpSession session = request.getSession();
        boolean flag;
        if (user.checkPassword(request.getParameter("password")) && user.checkUser(request.getParameter("login"))) {
            flag = true;
            session.setAttribute("flag", flag);
            filterChain.doFilter(servletRequest, servletResponse);
        } else {
            request.setAttribute("lable", "Invalid login or password. Please try again");
            request.getRequestDispatcher("/index.jsp").forward(request, response);
        }
    }

    @Override
    public void destroy() {

    }
}
