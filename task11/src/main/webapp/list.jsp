<%--
  Created by IntelliJ IDEA.
  User: Sasha
  Date: 18.06.2019
  Time: 14:17
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>ListOfBooks</title>
</head>
<body>
<%@ page isELIgnored="false" %>
<p>
<h1>List of books</h1></p>
<ul>
    <c:forEach items="${books}" var="books">
        <li>${books.toString()}</li>
    </c:forEach>
</ul>
</body>
</html>
