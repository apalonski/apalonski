package com.asoi.palonskiy.dao;

import com.asoi.palonskiy.model.Book;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class BookDaoImpl implements BookDao {

    private static final String DB_URL = "jdbc:h2:mem:test;DB_CLOSE_DELAY=-1";
    private static final String USER = "sa";
    private static final String PASS = "";
    private static final Logger LOGGER = LoggerFactory.getLogger(BookDaoImpl.class);
    private SessionFactory sessionFactory;

    public BookDaoImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public List<Book> getData() {
        List<Book> list = new ArrayList<>();
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        list.addAll(session.createQuery("from Book").list());
        session.getTransaction().commit();
        session.close();
        return list;
    }

    public void insert(Book book) {

        Session session = sessionFactory.openSession();
        session.beginTransaction();
        session.save(book);
        session.getTransaction().commit();
        session.close();
    }


    public void update(Book book) {
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        session.update(book);
        session.getTransaction().commit();
        session.close();

    }
}
