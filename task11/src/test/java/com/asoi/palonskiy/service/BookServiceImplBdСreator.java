package com.asoi.palonskiy.service;

import com.asoi.palonskiy.model.Book;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class BookServiceImplBdСreator {
    private List<Book> list = new ArrayList<>();
    private List<Book> expected = new ArrayList<>();
    BookServiceImpl bookServiceImpl;

    @Before
    public void initialization() {
        list.add(new Book(1, "Book1", "date1", "author1", "publisher1"));
        list.add(new Book(2, "Book2", "date2", "author2", "publisher2"));
        list.add(new Book(3, "Book3", "date3", "author3", "publisher3"));
        list.add(new Book(4, "Book4", "date4", "author4", "publisher4"));
        bookServiceImpl = new BookServiceImpl(list);
        expected.add(new Book(1, "Book1", "date1", "author1", "publisher1"));

    }

    @Test
    public void search() {
        List<Book> actual = bookServiceImpl.search("Book1");
        assertArrayEquals(expected.toArray(), actual.toArray());
    }

    @Test
    public void searchByAuthor() {
        List<Book> actual = bookServiceImpl.searchByAuthor("AuThOr1");
        assertArrayEquals(expected.toArray(), actual.toArray());
    }
}