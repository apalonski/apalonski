package com.asoi.palonskiy.service;

import com.asoi.palonskiy.dao.BookDao;
import com.asoi.palonskiy.model.Book;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class BookServiceImpl implements BookService {
    private List<Book> list;
    private List<Book> correct;
    private static final Logger LOGGER = LoggerFactory.getLogger(BookServiceImpl.class);
    private BookDao bookDao;

    public BookServiceImpl(BookDao bookDao) {
        this.bookDao = bookDao;
    }


    public List<Book> search(String name) {
        list = bookDao.getData();
        correct = list.stream()
                .filter(book -> book.getName().equals(name))
                .collect(Collectors.toList());
        LOGGER.info("Searching by name {}", name);
        LOGGER.info("Search was successful");
        return correct;
    }

    public List<Book> searchByAuthor(String name) {
        correct = list.stream()
                .filter(book -> book.getAuthor().toLowerCase().contains(name.toLowerCase()))
                .collect(Collectors.toList());
        LOGGER.info("Searching by name {}", name);
        LOGGER.info("Search was successful");
        return correct;
    }

    @Override
    public List<Book> getList() {
        return bookDao.getData();
    }

    @Override
    public void update(long id, HttpServletRequest req) {
        bookDao.update(bookFormer(id, req));
    }

    @Override
    public void insert(long id, HttpServletRequest req) {
        bookDao.insert(bookFormer(id, req));
    }

    @Override
    public Book bookFormer(long id, HttpServletRequest req) {
        return new Book(id, req.getParameter("name"), req.getParameter("date"),
                req.getParameter("author"), req.getParameter("publisher"));
    }
}
