package com.asoi.palonskiy.dbutil;

import com.asoi.palonskiy.model.Book;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


import javax.annotation.PostConstruct;

@Component
public class BdInitializer {

    private Logger logger = LoggerFactory.getLogger(BdInitializer.class);

    @Autowired
    private SessionFactory sessionFactory;

    @PostConstruct
    public void getDriver() {
        try {
            Class.forName("org.h2.Driver");
            fillDb();
        } catch (ClassNotFoundException e) {
            logger.error(e.getMessage());
        }
    }

    public void fillDb() {
        Book book1 = new Book(1L, "Book1", "author1", "2001-01-01", "publisher1");
        Book book2 = new Book(2L, "Book2", "author2", "2002-02-02", "publisher2");
        Book book3 = new Book(3L, "Book3", "author3", "2003-03-03", "publisher3");
        Book book4 = new Book(4L, "Book4", "author4", "2004-04-04", "publisher4");
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        session.save(book1);
        session.save(book2);
        session.save(book3);
        session.save(book4);
        session.getTransaction().commit();
        session.close();
    }

}
