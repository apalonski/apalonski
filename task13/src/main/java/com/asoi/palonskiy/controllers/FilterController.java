package com.asoi.palonskiy.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;



@Controller
@RequestMapping("/filterServlet")
public class FilterController {


    @PostMapping
    public String getMain() {
        return "main";
    }
}
