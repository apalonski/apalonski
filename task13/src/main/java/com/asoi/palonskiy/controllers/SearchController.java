/*
package com.asoi.palonskiy.controllers;

import com.asoi.palonskiy.model.Book;
import com.asoi.palonskiy.service.BookService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;


import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
@RequestMapping("/books/search")

public class SearchController {
    private BookService bookService;

    public SearchController(BookService bookService){
        this.bookService = bookService;
    }

    @GetMapping
    public String doSearch(HttpServletRequest req){
        String name = req.getParameter("name");
        List<Book> correct = bookService.search(name);
        req.setAttribute("correct", correct);
        check(correct, req);
        return "searchResult";
    }

    public void check(List<Book> list, HttpServletRequest req) {
        if (list.isEmpty()) {
            req.setAttribute("correct", "The book wasn't found.");
        }
    }
}
*/
