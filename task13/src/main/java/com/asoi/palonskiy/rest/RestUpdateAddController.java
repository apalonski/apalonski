package com.asoi.palonskiy.rest;

import com.asoi.palonskiy.model.Book;
import com.asoi.palonskiy.service.BookService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("/books/updateAdd")
public class RestUpdateAddController {

    private BookService bookService;

    public RestUpdateAddController(BookService bookService) {
        this.bookService = bookService;
    }

    @PutMapping
    public ResponseEntity<List<Book>> doAdd(HttpServletRequest req){
        long id = (long) bookService.getList().size() + 1;
        bookService.insert(id, req);
        List<Book> list = bookService.getList();
        return new ResponseEntity<>(list, HttpStatus.CREATED);
    }

    @PostMapping
    public ResponseEntity<List<Book>> doUpdate(HttpServletRequest req){
        long id = 1;
        bookService.update(id, req);
        List<Book> list = bookService.getList();
        return new ResponseEntity<>(list, HttpStatus.OK);
    }



}
