<%--
  Created by IntelliJ IDEA.
  User: Sasha
  Date: 18.06.2019
  Time: 15:59
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <title></title>
    <style type="text/css">
        .center {
            width: 20%;
            margin-top: 10%;
            background-color: #c9fdff;
        }
        .button {
            margin-bottom: 20px;
        }
        .img {
            width: 60%;
            height: 15%;
        }
    </style>
</head>
<body>
<div class="container center">
    <form action="books/search" method="get">
        <div class="row  text-center">
            <div class="col-sm ">
                <div>
                    <img class="img" src="images/book.png">
                </div>
                <div>
                    <input type="text" placeholder="Name" name="name" class="form-control"/></p>
                </div>
                <div class="button">
                    <input type="submit" name="Ok" value="Ok" class="btn btn-primary btn-lg btn-block"/>
                </div>
            </div>
        </div>
    </form>
</div>
</body>
</html>
