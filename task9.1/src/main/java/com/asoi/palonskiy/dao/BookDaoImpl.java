package com.asoi.palonskiy.dao;

import com.asoi.palonskiy.connection.BdConnection;
import com.asoi.palonskiy.model.Book;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class BookDaoImpl implements BookDao {

    private static final Logger LOGGER = LoggerFactory.getLogger(BookDaoImpl.class);
    private BdConnection bdConnection;

    public BookDaoImpl(BdConnection bdConnection) {
        this.bdConnection = bdConnection;
    }

    public List<Book> getData() {
        List<Book> list = new ArrayList<>();
        String sql = "SELECT id, name, author, date, publisher FROM IN_MEM_BOOK";
        try (Statement stmt = bdConnection.getConnection().createStatement(); ResultSet rs = stmt.executeQuery(sql);) {
            while (rs.next()) {
                int id = rs.getInt("id");
                String name = rs.getString("name");
                String author = rs.getString("author");
                String date = rs.getString("date");
                String publisher = rs.getString("publisher");
                list.add(new Book(id, name, author, date, publisher));
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }
        return list;
    }

    public void insert(Book book) {

        try(Statement stmt = bdConnection.getConnection().createStatement();) {
            String sql = "INSERT INTO IN_MEM_BOOK " + String.format("VALUES (%d, '%s', '%s', TO_DATE('%s', 'yyyy-mm-dd'), '%s')",
                    book.getId(), book.getName(), book.getAuthor(), book.getDate(), book.getPublisher());
            stmt.executeUpdate(sql);
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }
    }

    public long getLastId() {
        String sql = "SELECT TOP 1 id FROM IN_MEM_BOOK ORDER BY id DESC";
        long id = 0;
        try(Statement stmt = bdConnection.getConnection().createStatement();ResultSet rs = stmt.executeQuery(sql);) {
            while (rs.next()) {
                id = rs.getInt("id");
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }
        return id;
    }

    public void update(Book book) {
        try(Statement stmt = bdConnection.getConnection().createStatement();) {
            String updateLine = String.format("SET name = '%s' , author='%s', date=TO_DATE('%s', 'yyyy-mm-dd'), publisher = '%s'  WHERE id = %d"
                    , book.getName(), book.getAuthor(), book.getDate(), book.getPublisher(), book.getId());
            String sql = "UPDATE IN_MEM_BOOK " + updateLine;
            stmt.executeUpdate(sql);
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }

    }
}
