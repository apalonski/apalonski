package com.asoi.palonskiy.configuration;

import com.asoi.palonskiy.connection.BdConnection;
import com.asoi.palonskiy.dao.BookDao;
import com.asoi.palonskiy.dao.BookDaoImpl;
import com.asoi.palonskiy.dao.UsersDao;
import com.asoi.palonskiy.dao.UsersDaoImpl;
import com.asoi.palonskiy.service.BookServiceImpl;
import com.asoi.palonskiy.service.UserServiceImpl;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;



@Configuration
@PropertySource("classpath:bd/bd.properties")
public class AppConfig {

    @Value("${db.url}")
    private String url;
    @Value("${db.login}")
    private String login;
    @Value("${db.password}")
    private String password;
    @Bean
    public BdConnection getConnection(){
        return new BdConnection(url, login, password);
    }
    @Bean
    public BookDaoImpl bookDaoImpl(BdConnection bdConnection){
        return new BookDaoImpl(bdConnection);
    }
    @Bean
    public UsersDaoImpl usersDaoImpl(BdConnection bdConnection){
        return new UsersDaoImpl(bdConnection);
    }
    @Bean
    public BookServiceImpl bookServiceImpl(BookDao bookDao){
        return new BookServiceImpl(bookDao);
    }
    @Bean
    public UserServiceImpl userServiceImpl(UsersDao usersDao){
        return new UserServiceImpl(usersDao);
    }
}
