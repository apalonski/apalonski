package com.asoi.palonskiy;

import java.util.Scanner;

public class Zd1 {
    public static String err = "Проверьте введенные данные";

    public static void main(String[] args) {
        menu();
    }
    public static void menu() {
        while (true) {
            try {
                Scanner input = new Scanner(System.in);
                System.out.println("Введите 1, если хотите создать карту с балансом\nВведите 2, если хотите создать карту без баланса");
                int choice1 = input.nextInt();
                if (choice1 == 1) {
                    menu2(constr1());
                    break;
                } else if (choice1 == 2) {

                    menu2(constr2());
                    break;
                } else {
                    throw new Exception();
                }
            } catch (Exception ex) {
                System.out.println("Выберете нужный вариант");
            }
        }
    }

    public static Card constr1() {
        Card crd1;
        while (true) {
            try {
                Scanner input1 = new Scanner(System.in);
                System.out.println("Введите баланс");
                double balance = input1.nextDouble();
                if (balance < 0) {
                    throw new Exception();
                }
                Scanner input2 = new Scanner(System.in);
                System.out.println("Введите имя");
                String name = input2.nextLine();
                crd1 = new Card(name, balance);
                System.out.println(crd1.getname() + " " + crd1.getbalance());
                break;
            } catch (Exception ex) {
                System.out.println(err);
            }
        }
        return crd1;
    }

    public static Card constr2() {
        Scanner input = new Scanner(System.in);
        System.out.println("Введите имя");
        String name = input.nextLine();
        Card crd1 = new Card(name);
        System.out.println(crd1.getname() + " " + crd1.getbalance());
        return crd1;
    }

    public static void menu2(Card crd) {
        while (true) {
            try {
                Scanner input = new Scanner(System.in);
                System.out.println("Введите 1, если хотите зачислить средства\nВведите 2, если хотите снять средства");
                System.out.println("Введите 3, если хотите перевести средства\nВведите 4, если хотите узнать состояние счета");
                System.out.println("Введите 5, если хотите выйти");
                int choice2 = input.nextInt();
                if (choice2 == 1) {
                    double numb = input();
                    if (numb < 0) {
                        throw new Exception();
                    }
                    crd.increase(numb);
                } else if (choice2 == 2) {
                    double numb = input();
                    if (numb > crd.getbalance() || numb < 0) {
                        System.out.println("Недостаточно средств,\nвведите корректную сумму");
                        throw new Exception();
                    }
                    crd.decrease(numb);
                } else if (choice2 == 3) {
                    double numb = input();
                    if (numb < 0) {
                        throw new Exception();
                    }
                    crd.converting(numb);
                } else if (choice2 == 4) {
                    System.out.println(crd.getname() + " " + crd.getbalance());
                } else if (choice2 == 5) {
                    break;
                } else {
                    throw new Exception();
                }
            } catch (Exception ex) {
                System.out.println("Повторите ввод");
            }
        }
    }
    public static double input (){
        Scanner input = new Scanner(System.in);
        System.out.println("Введите число");
        double numb = input.nextDouble();
        return numb;
    }
}
