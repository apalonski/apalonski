package com.asoi.palonskiy;

import java.util.Scanner;

public class Card {
    private String name;
    private double balance;

    public Card(String nam) {
        this(nam, 0);
    }

    public Card(String nam, double bal) {
        name = nam;
        balance = bal;
    }

    public String getname() {
        return name;
    }

    public double getbalance() {
        return balance;
    }

    public void increase(double numb) {
        if (numb >= 0) {
            balance += numb;
        }

    }

    public void decrease(double numb) {
        if (numb < balance && numb >= 0) {
            balance -= numb;
        }
    }

    public void converting(double numb) {
        if (numb > 0) {
            balance *= numb;
        }
    }
}
