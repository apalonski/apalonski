package com.asoi.palonskiy;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class Med {
    public static int [] masInt (){
        Scanner input = new Scanner(System.in);
        System.out.println("Размерность 1-ого массива");
        int x1 = input.nextInt();
        int[] mas_int = new int[x1];
        Random rnd = new Random(System.currentTimeMillis());
        for (int i = 0; i < mas_int.length; i++) {
            mas_int[i] = rnd.nextInt(11);
            System.out.print(mas_int[i] + " ");
        }
        System.out.println("");
        return mas_int;
    }
    public  static double [] masDouble (){
        Scanner input = new Scanner(System.in);
        System.out.println("Размерность 2-ого массива");
        int x2 = input.nextInt();
        double[] mas_double = new double[x2];
        Random rnd = new Random(System.currentTimeMillis());

        for (int i = 0; i < mas_double.length; i++) {
            mas_double[i] = rnd.nextDouble() * 10 + 1;
            System.out.printf("%.2f ", mas_double[i]);
        }
        System.out.println("");
        return  mas_double;
    }
    public static double med(int[] mas) {
        Arrays.sort(mas);
        double med = 0;
        if (mas.length % 2 == 0) {
            med = (mas[mas.length / 2 - 1] + mas[mas.length / 2]) / 2.0;
        } else {
            med = mas[mas.length / 2];
        }

        return med;
    }

    public static double med(double[] mas) {
        Arrays.sort(mas);
        double med = 0;
        if (mas.length % 2 == 0) {
            med = (mas[mas.length / 2 - 1] + mas[mas.length / 2]) / 2.0;
        } else {
            med = mas[mas.length / 2];
        }
        return med;
    }
}
