package com.asoi.palonskiy;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CardTest {

    @Test
    void increase() {
        Card crd1 = new Card ("Sanya",322);
        crd1.increase(100);
        assertEquals(422,crd1.getbalance());
    }

    @Test
    void decrease() {
        Card crd1 = new Card ("Sanya",322);
        crd1.decrease(100);
        assertEquals(222,crd1.getbalance());
    }

    @Test
    void converting() {
        Card crd1 = new Card ("Sanya",322);
        crd1.converting(2);
        assertEquals(644,crd1.getbalance());
    }
    @Test
    void decreaseMin() {
        Card crd1 = new Card ("Sanya");
        crd1.decrease(100);
        assertEquals(0,crd1.getbalance());
    }
    @Test
    void increaseMin() {
        Card crd1 = new Card ("Sanya");
        crd1.increase(-100);
        assertEquals(0,crd1.getbalance());
    }
    @Test
    void convertingMin() {
        Card crd1 = new Card ("Sanya",100);
        crd1.converting(-0.5);
        assertEquals(100,crd1.getbalance());
    }
}