<%--
  Created by IntelliJ IDEA.
  User: Sasha
  Date: 18.06.2019
  Time: 14:17
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page isELIgnored="false" %>
<html>
<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <title></title>
    <style type="text/css">
        .center {
            width: 30%;
            margin-top: 10%;
            background-color: #c9fdff;
        }

        .colorSpan {
            color: black;
        }
    </style>
</head>
<body>
<div class="container center">
    <p class="h1 text-primary"> List of books</p>
    <div class="row">
        <ul>
            <c:forEach items="${books}" var="books">
                <li class="text-primary">
        			<span class="colorSpan">
                            ${books.toString()}
                    </span>
                </li>
            </c:forEach>
        </ul>
    </div>
</div>
</body>
</html>
