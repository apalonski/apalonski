package com.asoi.palonskiy.mapper;

import com.asoi.palonskiy.model.User;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class UserMapper implements RowMapper<User> {
    @Override
    public User mapRow(ResultSet resultSet, int i) throws SQLException {
        return new User(resultSet.getString("login"), resultSet.getString("password"));
    }
}
