package com.asoi.palonskiy.mapper;

import com.asoi.palonskiy.model.Book;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class BookMapper implements RowMapper<Book> {
    @Override
    public Book mapRow(ResultSet resultSet, int i) throws SQLException {
        return new Book(resultSet.getLong("id"),resultSet.getString("name"),
                resultSet.getString("date"),resultSet.getString("author"),
                resultSet.getString("publisher"));
    }
}
