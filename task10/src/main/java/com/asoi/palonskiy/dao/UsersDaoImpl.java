package com.asoi.palonskiy.dao;

import com.asoi.palonskiy.model.User;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;


import java.util.ArrayList;
import java.util.List;

@Repository
public class UsersDaoImpl implements UsersDao {

    @Autowired
    private SessionFactory sessionFactory;

    public List<User> getUser() {
        List<User> list = new ArrayList<>();
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        list.addAll(session.createQuery("from User").list());
        session.getTransaction().commit();
        session.close();
        return list;
    }

}
