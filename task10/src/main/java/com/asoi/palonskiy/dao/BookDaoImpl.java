package com.asoi.palonskiy.dao;

import com.asoi.palonskiy.model.Book;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class BookDaoImpl implements BookDao {

    @Autowired
    private SessionFactory sessionFactory;

    public List<Book> getData() {
        List<Book> list = new ArrayList<>();
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        list.addAll(session.createQuery("from Book").list());
        session.getTransaction().commit();
        session.close();
        return list;
    }

    public void insert(Book book) {
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        session.save(book);
        session.getTransaction().commit();
        session.close();
    }


    public void update(Book book) {
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        session.update(book);
        session.getTransaction().commit();
        session.close();
    }
}
