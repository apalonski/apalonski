package com.asoi.palonskiy.service;

import com.asoi.palonskiy.model.Book;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface BookService {
    List<Book> search(String name);

    List<Book> searchByAuthor(String name);

    List<Book> getList();

    void update(long id, HttpServletRequest req);

    void insert(long id, HttpServletRequest req);

    Book bookFormer(long id, HttpServletRequest req);

}
