package com.asoi.palonskiy.service;

public interface UserService {
    boolean checkUser(String name);

    boolean checkPassword(String name);
}
