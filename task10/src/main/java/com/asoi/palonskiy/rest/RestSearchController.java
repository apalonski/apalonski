package com.asoi.palonskiy.rest;

import com.asoi.palonskiy.model.Book;
import com.asoi.palonskiy.service.BookService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("/books/search")

public class RestSearchController {
    private BookService bookService;

    public RestSearchController(BookService bookService){
        this.bookService = bookService;
    }

    @GetMapping
    public ResponseEntity<List<Book>> doSearch(HttpServletRequest req){
        String name = req.getParameter("name");
        List<Book> correct = bookService.search(name);
        req.setAttribute("correct", correct);
        check(correct, req);
        return new ResponseEntity<>(correct, HttpStatus.OK);
    }

    private void check(List<Book> list, HttpServletRequest req) {
        if (list.isEmpty()) {
            req.setAttribute("correct", "The book wasn't found.");
        }
    }
}
