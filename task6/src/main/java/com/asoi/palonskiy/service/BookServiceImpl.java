package com.asoi.palonskiy.service;

import com.asoi.palonskiy.model.Book;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.stream.Collectors;

public class BookServiceImpl implements BookService {
    private List<Book> list;
    private List<Book> correct;
    private static final Logger LOGGER = LoggerFactory.getLogger(BookServiceImpl.class);

    public BookServiceImpl(List<Book> list) {
        this.list = list;
    }


    public List<Book> search(String name) {
        correct = list.stream()
                .filter(book -> book.getName().equals(name))
                .collect(Collectors.toList());
        LOGGER.info("Searching by name {}",name);
        LOGGER.info("Search was successful");
        return correct;
    }

    public List<Book> searchByAuthor(String name) {
        correct = list.stream()
                .filter(book -> book.getAuthor().toLowerCase().contains(name.toLowerCase()))
                .collect(Collectors.toList());
        LOGGER.info("Searching by name {}",name);
        LOGGER.info("Search was successful");
        return correct;
    }


}
