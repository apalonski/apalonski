package com.asoi.palonskiy.controllers;

import com.asoi.palonskiy.dao.BookDao;
import com.asoi.palonskiy.dao.BookDaoImpl;
import com.asoi.palonskiy.model.Book;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

@WebServlet("/books/updateAdd")
public class UpdateAddServlet extends HttpServlet {
    private static final String BOOKS = "books";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        BookDao bookDao = new BookDaoImpl();
        long id = bookDao.getLastId()+1;
        bookDao.insert(id, req.getParameter("name"), req.getParameter("date"), req.getParameter("author"), req.getParameter("publisher"));
        List<Book> list = bookDao.getData();
        HttpSession session = req.getSession();
        session.setAttribute(BOOKS, list);
        req.getRequestDispatcher("/list.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();
        BookDao bookDao = new BookDaoImpl();
        long id = Long.parseLong((session.getAttribute("id")).toString());
        bookDao.update(id, req.getParameter("name"), req.getParameter("date"), req.getParameter("author"), req.getParameter("publisher"));
        List<Book> list = bookDao.getData();
        session.setAttribute(BOOKS, list);
        req.getRequestDispatcher("/list.jsp").forward(req, resp);
    }


}
