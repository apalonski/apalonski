package com.asoi.palonskiy.controllers;

import com.asoi.palonskiy.dao.BookDao;
import com.asoi.palonskiy.dao.BookDaoImpl;
import com.asoi.palonskiy.model.Book;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

@WebServlet("/books")
public class MainServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        BookDao bookDao = new BookDaoImpl();
        List<Book> list= bookDao.getData();
        HttpSession session = req.getSession();
        try {
            if (req.getParameter("ListOfBooks") != null) {
                req.setAttribute("books", list);
                getServletContext().getRequestDispatcher("/list.jsp").forward(req, resp);
            } else if (req.getParameter("Search") != null) {
                getServletContext().getRequestDispatcher("/search.jsp").forward(req, resp);
            } else if (req.getParameter("Update/Add") != null) {
                if (req.getParameter("id").equals("new")) {
                    getServletContext().getRequestDispatcher("/add.jsp").forward(req, resp);
                } else if (Integer.parseInt(req.getParameter("id")) != 0 && (Integer.parseInt(req.getParameter("id")) <= list.size())) {
                    session.setAttribute("id", req.getParameter("id"));
                    getServletContext().getRequestDispatcher("/update.jsp").forward(req, resp);

                } else throw new NumberFormatException();

            }
        } catch (NumberFormatException ex) {
            getServletContext().getRequestDispatcher("/error.jsp").forward(req, resp);
        }

    }

}
