package com.asoi.palonskiy.dao;

import com.asoi.palonskiy.model.Book;

import java.util.List;

public interface BookDao {
    List<Book> getData();
    void insert(Long id, String name, String date, String author, String publisher);
    long getLastId();
    void update(long id, String name, String date, String author, String publisher);
}
