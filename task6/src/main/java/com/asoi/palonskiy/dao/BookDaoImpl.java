package com.asoi.palonskiy.dao;

import com.asoi.palonskiy.model.Book;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class BookDaoImpl implements BookDao {

    private static final String DB_URL = "jdbc:h2:mem:test;DB_CLOSE_DELAY=-1";
    private static final String USER = "sa";
    private static final String PASS = "";
    private static final Logger LOGGER = LoggerFactory.getLogger(BookDaoImpl.class);

    public List<Book> getData() {
        List<Book> list = new ArrayList<>();
        String sql = "SELECT id, name, author, date, publisher FROM IN_MEM_BOOK";
        try (Connection conn = DriverManager.getConnection(DB_URL, USER, PASS); Statement stmt = conn.createStatement(); ResultSet rs = stmt.executeQuery(sql);) {
            while (rs.next()) {
                int id = rs.getInt("id");
                String name = rs.getString("name");
                String author = rs.getString("author");
                String date = rs.getString("date");
                String publisher = rs.getString("publisher");
                list.add(new Book(id, name, author, date, publisher));
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }
        return list;
    }

    public void insert(Long id, String name, String date, String author, String publisher) {

        try (Connection conn = DriverManager.getConnection(DB_URL, USER, PASS); Statement stmt = conn.createStatement();) {
            String sql = "INSERT INTO IN_MEM_BOOK " + String.format("VALUES (%d, '%s', '%s', TO_DATE('%s', 'yyyy-mm-dd'), '%s')", id, name,author, date, publisher);
            stmt.executeUpdate(sql);
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }
    }

    public long getLastId() {
        String sql = "SELECT TOP 1 id FROM IN_MEM_BOOK ORDER BY id DESC";
        long id = 0;
        try (Connection conn = DriverManager.getConnection(DB_URL, USER, PASS); Statement stmt = conn.createStatement(); ResultSet rs = stmt.executeQuery(sql);) {
            while (rs.next()) {
                id = rs.getInt("id");
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }
        return id;
    }

    public void update(long id, String name, String date, String author, String publisher) {
        try (Connection conn = DriverManager.getConnection(DB_URL, USER, PASS); Statement stmt = conn.createStatement();) {
            String updateLine = String.format("SET name = '%s' , author='%s', date=TO_DATE('%s', 'yyyy-mm-dd'), publisher = '%s'  WHERE id = %d"
                    , name,author,  date, publisher, id);
            String sql = "UPDATE IN_MEM_BOOK " + updateLine ;
            stmt.executeUpdate(sql);
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }

    }
}
