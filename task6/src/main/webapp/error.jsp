<%--
  Created by IntelliJ IDEA.
  User: Sasha
  Date: 19.06.2019
  Time: 14:20
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Ошибка 404</title>
</head>
<body>
<img src="images/404-html.png" alt="Ошибка 404">
<p>К сожалению, запрашиваемая Вами страница не найдена..</p>
<p>Почему?</p>
<ol>
    <li>Вам просто не повезло.
    <li>Вы неправильно указали данные.
    <li>В поле Id должно быть указано или слово "new", для создания новой книги.<br>
        или Id уже существующей книги
</ol>
</body>
</html>
