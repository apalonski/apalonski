package com.asoi.palonskiy;

public class zd1 {
    public static void main(String [] args) {
        int a = Integer.parseInt(args[0]);
        int p = Integer.parseInt(args[1]);
        double m1 =Double.parseDouble(args[2]);
        double m2 = Double.parseDouble(args[3]);
        System.out.printf("g = %.2f ",get_g(a,p,m1,m2));
    }
    public static double get_g (double a,double p,double m1,double m2) {
        double g =4*Math.pow(Math.PI,2)*(Math.pow(a,3)/(Math.pow(p,2)*(m1+m2)));
        return g;
}

}
