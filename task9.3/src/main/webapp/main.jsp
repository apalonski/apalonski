<%--
  Created by IntelliJ IDEA.
  User: Sasha
  Date: 18.06.2019
  Time: 14:03
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page isELIgnored="false" %>
<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <title></title>
    <style type="text/css">
        .center {
            width: 30%;
            margin-top: 10%;
            background-color: #c9fdff;
        }
        .right {
            margin-right: 50px;
            margin-top: 50px;
            margin-bottom: 20px;
            width: 30%;
        }
        .logOut {
            position: absolute;
            top: 69%;
            left: 5%;

        }
        .img {
            width: 60%;
            height: 60%;
        }
    </style>
</head>
<body>
<div class="container center">
    <form action="books" method="get">
        <div class="row  text-center">
            <div class="col-sm ">
                <div>
                    <img class="img" src="<c:url value="/resources/book.png" />">
                </div>
                <div>
                    <input type="text" placeholder="ID книги" name="id"/></p>
                </div>
                <p class="logOut">
                    <input type="submit" name="LogOut" value="LogOut" class="btn btn-primary btn-lg"/>
                </p>
            </div>
            <div class=" right">
                <p>
                    <input type="submit" name="ListOfBooks" value="ListOfBooks"
                           class="btn btn-primary btn-lg btn-block"/>
                </p>
                <p>
                    <input type="submit" name="Search" value="Search" class="btn btn-primary btn-lg btn-block"/>
                </p>
                <p>
                    <input type="submit" name="Update/Add" value="Update/Add" class="btn btn-primary btn-lg btn-block"/>
                </p>
            </div>
        </div>
    </form>
</div>
</body>
</html>
