package com.asoi.palonskiy.dbutil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;


import javax.annotation.PostConstruct;
import javax.sql.DataSource;

@Component
public class BdCreatorAndFiller {

    private Logger logger = LoggerFactory.getLogger(BdCreatorAndFiller.class);
    private static final String INSERT_LINE = "INSERT INTO IN_MEM_BOOK ";

    private JdbcTemplate jdbcTemplate;

    public BdCreatorAndFiller(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    @PostConstruct
    public void getDriver() {
        try {
            Class.forName("org.h2.Driver");
            createBd();
            fillDb();
        } catch (ClassNotFoundException e) {
            logger.error(e.getMessage());
        }
    }

    public void createBd() {
        String sql = "CREATE TABLE   IN_MEM_BOOK " +
                "(id INTEGER not NULL, " +
                " name VARCHAR(255), " +
                " author VARCHAR(255), " +
                " date date , " +
                " publisher VARCHAR(255), " +
                " PRIMARY KEY ( id ))";
        jdbcTemplate.execute(sql);
        sql = "CREATE TABLE   USERS " +
                "(login VARCHAR(255), " +
                " password VARCHAR(255), " +
                " PRIMARY KEY ( login ))";
        jdbcTemplate.execute(sql);
    }

    public void fillDb() {
        String sql = INSERT_LINE + "VALUES (1, 'Book1','author1', TO_DATE('2001-01-01', 'yyyy-mm-dd') ,'publisher1')";
        jdbcTemplate.execute(sql);
        sql = INSERT_LINE + "VALUES (2, 'Book2','author2',  TO_DATE('2002-02-02', 'yyyy-mm-dd'), 'publisher2')";
        jdbcTemplate.execute(sql);
        sql = INSERT_LINE + "VALUES (3, 'Book3','author3',  TO_DATE('2003-03-03', 'yyyy-mm-dd'), 'publisher3')";
        jdbcTemplate.execute(sql);
        sql = INSERT_LINE + "VALUES (4, 'Book4','author4',  TO_DATE('2004-04-04', 'yyyy-mm-dd'), 'publisher4')";
        jdbcTemplate.execute(sql);
        sql = "INSERT INTO USERS " + "VALUES ('user1', 'user1')";
        jdbcTemplate.execute(sql);
        sql = "INSERT INTO USERS " + "VALUES ('user2', 'user1')";
        jdbcTemplate.execute(sql);

    }

}
