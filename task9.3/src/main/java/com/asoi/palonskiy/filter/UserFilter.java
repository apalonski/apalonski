package com.asoi.palonskiy.filter;


import com.asoi.palonskiy.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/*@WebFilter(urlPatterns = {"/filterServlet", "/books/*"})*/

@Component("filter")
public class UserFilter implements Filter {
    @Autowired
    private UserService userService;


    @Override
    public void init(FilterConfig filterConfig) {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        HttpSession session = request.getSession();
        boolean flag;
        if (session.getAttribute("flag") != null) {
            filterChain.doFilter(servletRequest, servletResponse);
        } else if (userService.checkPassword(request.getParameter("password")) && userService.checkUser(request.getParameter("login"))) {
            flag = true;
            session.setAttribute("flag", flag);
            filterChain.doFilter(servletRequest, servletResponse);
        }else if(session.getAttribute("flag") == null&&request.getParameter("password")==null&&request.getParameter("login")==null){
            request.getRequestDispatcher("/loginError.jsp").forward(request, response);
        } else {
            request.setAttribute("lable", "Invalid login or password. Please try again");
            request.getRequestDispatcher("/index.jsp").forward(request, response);
        }
    }

    @Override
    public void destroy() {

    }
}
