package com.asoi.palonskiy.exception;

public class MainServletExceptoin extends Exception {
    private static final String MESSAGE = "неправильно указанные данные.\n" +
            "поле Id должно быть указано или слово \"new\", для создания новой книги\n" +
            "или Id уже существующей книги";

    @Override
    public String getMessage() {
        return MESSAGE;
    }

    public MainServletExceptoin() {
        super();
    }
}
