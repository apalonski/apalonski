package com.asoi.palonskiy.dao;

import com.asoi.palonskiy.mapper.UserMapper;
import com.asoi.palonskiy.model.User;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;


import javax.sql.DataSource;
import java.util.List;

@Repository
public class UsersDaoImpl implements UsersDao {

    private JdbcTemplate jdbcTemplate;

    public UsersDaoImpl(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    public List<User> getUser() {
        return jdbcTemplate.query("select * from USERS", new UserMapper());
    }

}
