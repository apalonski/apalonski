package com.asoi.palonskiy.dao;

import com.asoi.palonskiy.mapper.BookMapper;
import com.asoi.palonskiy.model.Book;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.util.List;

@Repository
public class BookDaoImpl implements BookDao {


    private JdbcTemplate jdbcTemplate;

    public BookDaoImpl(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    public List<Book> getData() {
        return jdbcTemplate.query("select * from IN_MEM_BOOK", new BookMapper());
    }

    public void insert(Book book) {
        String sql = "INSERT INTO IN_MEM_BOOK " + String.format("VALUES (%d, '%s', '%s', TO_DATE('%s', 'yyyy-mm-dd'), '%s')",
                book.getId(), book.getName(), book.getAuthor(), book.getDate(), book.getPublisher());
        jdbcTemplate.update(sql);
    }


    public void update(Book book) {
        String updateLine = String.format("SET name = '%s' , author='%s', date=TO_DATE('%s', 'yyyy-mm-dd'), publisher = '%s'  WHERE id = %d"
                , book.getName(), book.getAuthor(), book.getDate(), book.getPublisher(), book.getId());
        String sql = "UPDATE IN_MEM_BOOK " + updateLine;
        jdbcTemplate.update(sql);
    }
}
