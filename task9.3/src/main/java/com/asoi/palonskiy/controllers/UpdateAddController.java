package com.asoi.palonskiy.controllers;

import com.asoi.palonskiy.model.Book;
import com.asoi.palonskiy.service.BookService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
@RequestMapping("/books/updateAdd")
public class UpdateAddController{
    private static final String BOOKS = "books";

    private BookService bookService;

    public UpdateAddController(BookService bookService) {
        this.bookService = bookService;
    }

    @GetMapping
    public String doAdd(HttpServletRequest req){
        long id = (long) bookService.getList().size() + 1;
        bookService.insert(id, req);
        List<Book> list = bookService.getList();
        HttpSession session = req.getSession();
        session.setAttribute(BOOKS, list);
        return "list";
    }

    @PostMapping
    public String doUpdate(HttpServletRequest req){
        HttpSession session = req.getSession();
        long id = Long.parseLong((session.getAttribute("id")).toString());
        bookService.update(id, req);
        List<Book> list = bookService.getList();
        session.setAttribute(BOOKS, list);
        return "list";
    }



}
