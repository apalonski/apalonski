package com.asoi.palonskiy.file;

import java.util.List;

public interface FileSystem {

    void print();

    void addFolderComponent(FileSystem folderComponent);

    void setFolderName(String name);

    String getFolderName();

    void setFlag(int nesting);

    int getNesting();

    List<FileSystem> getFolderComponents();
}
