package com.asoi.palonskiy.file;

import java.util.List;

public class File implements FileSystem {
    private String folderName;
    private int nesting;

    public File() {
    }

    public void addFolderComponent(FileSystem folderComponent) {
        System.out.println("File can't have child folders or files!");
    }

    public void setFolderName(String name) {
        this.folderName = name;
    }

    public String getFolderName() {
        return this.folderName;
    }

    public void setFlag(int nesting) {
        this.nesting = nesting;
    }

    public int getNesting() {
        return this.nesting;
    }

    public List<FileSystem> getFolderComponents() {
        return null;
    }

    public void print() {
        String defaultTabulation = "   ";
        String actualTabulation = tabulationMultiply(defaultTabulation, this.getNesting());
        System.out.println(actualTabulation + this.folderName);
    }

    public static String tabulationMultiply(String s, int n) {
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < n; ++i) {
            sb.append(s);
        }

        return sb.toString();
    }
}
