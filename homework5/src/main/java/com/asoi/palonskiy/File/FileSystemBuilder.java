package com.asoi.palonskiy.file;

import com.asoi.palonskiy.folder.Folder;
import com.asoi.palonskiy.folder.Folder;

import java.util.List;

public class FileSystemBuilder {

    public static void updateFileSystem(List<String> splittedListOfLines, FileSystem currentFileSystem, int flag) {
        List<FileSystem> currentFolders = currentFileSystem.getFolderComponents();
        boolean hasMatch = true;
        boolean firstLevel = false;
        if (currentFolders != null){
            if (currentFolders.size() > 0){
                for (int i = 0; i < currentFolders.size(); i++){
                    for (int y = 0; y < splittedListOfLines.size(); y++){
                        String currentFolderName = currentFolders.get(i).getFolderName();
                        String currentInputName = splittedListOfLines.get(y);
                        if (currentFolderName.equals(currentInputName)){
                            splittedListOfLines.remove(y);
                            flag++;
                            FileSystem currentFolderComponent = currentFolders.get(i);
                            updateFileSystem(splittedListOfLines, currentFolderComponent, flag);
                        } else {
                            hasMatch = false;
                        }
                    }
                }
            } else {
                firstLevel = true;
            }
        } else {
            System.out.println("Current directory already existing");
        }


        if (!hasMatch){
            createFileSystem(splittedListOfLines, currentFileSystem, flag);
        }
        if (firstLevel){
            createFileSystem(splittedListOfLines, currentFileSystem, flag);
        }
    }

    public static void createFileSystem(List<String> splittedListOfLines, FileSystem currentFileSystem, int flag) {
        for (int i = 0; i < splittedListOfLines.size(); i++){
            FileSystem folderComponent;
            if (!splittedListOfLines.get(i).contains(".")){
                folderComponent = new Folder();
            } else {
                folderComponent = new File();

            }
            folderComponent.setFolderName(splittedListOfLines.get(i));
            folderComponent.setFlag(flag);
            currentFileSystem.addFolderComponent(folderComponent);

            splittedListOfLines.remove(i);
            flag++;

            createFileSystem(splittedListOfLines, folderComponent, flag);
        }
    }
}
