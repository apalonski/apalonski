package com.asoi.palonskiy.input;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Validator {
    public static String checkInput(List<String> splittedListOfLines) {
        String validator = "Ok";
        Pattern folderReg = Pattern.compile("[.#%&*|:<>?]");
        Pattern fileReg = Pattern.compile("[#%&*|:<>?]");

        for (int i = 0; i < splittedListOfLines.size(); i++){
            if (splittedListOfLines.get(i).contains(".") && !splittedListOfLines.get(i).equals(splittedListOfLines.get(splittedListOfLines.size() - 1))){
                validator = "can't have subfolders or subfiles";
                break;
            } else if (splittedListOfLines.get(i).contains(".") && splittedListOfLines.get(i).equals(splittedListOfLines.get(splittedListOfLines.size() - 1))){
                int dotsCount = 0;
                for (int y = 0; y < splittedListOfLines.get(i).length(); y++){
                    if (splittedListOfLines.get(i).charAt(y) == '.'){
                        dotsCount++;
                    }
                }
                Matcher checkFile = fileReg.matcher(splittedListOfLines.get(i));
                if (checkFile.find() || dotsCount > 1){
                    validator = "Invalid file name: " + splittedListOfLines.get(i);
                    break;
                }
            } else if (!splittedListOfLines.get(i).contains(".")){
                Matcher checkFolder = folderReg.matcher(splittedListOfLines.get(i));
                if (checkFolder.find()){
                    validator = "Invalid folder name: " + splittedListOfLines.get(i);
                    break;
                }
            }
        }

        return validator;
    }
}
