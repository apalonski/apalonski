package com.asoi.palonskiy.input;

import com.asoi.palonskiy.file.FileSystem;
import com.asoi.palonskiy.file.FileSystemBuilder;
import com.asoi.palonskiy.folder.Folder;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Input {
    public static void parseInputRoot(){
        System.out.println("Not available symbols: .#%&*|:<>?");
        System.out.println("Enter \"close\" for close");
        System.out.println("Enter \"print\" for print");

        FileSystem currentFileSystem = new Folder();//
        int flag = 0;//
        Scanner scanner = new Scanner(System.in);
        String inputString = scanner.nextLine();

        while (!inputString.equals("close")){
            if (inputString.equals("print")){
                currentFileSystem.print();
            } else {
                String[] splittedMasOfLines = inputString.split("(?<=/)");
                List<String> splittedListOfLines = new ArrayList<String>();
                for (int i = 0; i < splittedMasOfLines.length; i++){
                    if (!splittedMasOfLines[i].contains(".") && !splittedMasOfLines[i].contains("/")){
                        splittedMasOfLines[i] = splittedMasOfLines[i] + "/";
                    }
                    splittedListOfLines.add(i, splittedMasOfLines[i]);
                }

                String validator = Validator.checkInput(splittedListOfLines);//
                if (validator.equals("Ok")){
                    FileSystemBuilder.updateFileSystem(splittedListOfLines, currentFileSystem, flag);
                } else {
                    System.out.println(validator);
                    if (currentFileSystem.getFolderComponents().size() == 0){
                        System.out.println("File system wasn't created");
                    } else {
                        System.out.println("system wasn't updated");
                    }
                }
            }
            inputString = scanner.nextLine();
        }
    }
}
