package com.asoi.palonskiy.folder;

import com.asoi.palonskiy.file.FileSystem;

import java.util.ArrayList;
import java.util.List;

public class Folder implements FileSystem {
    private String folderName;
    private int nesting;
    private List<FileSystem> folderComponents = new ArrayList<>();

    public void addFolderComponent(FileSystem folderComponent){
        folderComponents.add(folderComponent);
    }

    public List<FileSystem> getFolderComponents(){
        return folderComponents;
    }

    public void setFolderName(String name) {
        this.folderName = name;
    }

    public String getFolderName() {
        return this.folderName;
    }

    public void setFlag(int nesting) {
        this.nesting = nesting;
    }

    public int getNesting() {
        return this.nesting;
    }

    public void print() {
        for (FileSystem folderComponent: folderComponents){
            if (folderComponent instanceof Folder){
                String defaultTabulation = "   ";
                String actualTabulation = tabulationMultiply(defaultTabulation, (folderComponent).getNesting());
                System.out.println(actualTabulation + folderComponent.getFolderName());
            }
            folderComponent.print();
        }
    }

    public static String tabulationMultiply(String s, int n){
        StringBuilder sb = new StringBuilder();
        for(int i = 0; i < n; i++){
            sb.append(s);
        }
        return sb.toString();
    }
}

