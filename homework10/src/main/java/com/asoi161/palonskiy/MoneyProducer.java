package com.asoi161.palonskiy;

public class MoneyProducer implements Runnable {
    Card card;

    public MoneyProducer(Card card) {
        this.card = card;
    }

    @Override
    public void run() {
        System.out.printf("%s started... \n", Thread.currentThread().getName());
        try {
            while (card.getBalance() < 1000) {
                card.deposit(5);
                Thread.sleep(5000);
            }
        } catch (InterruptedException e) {
            System.out.println("Thread has been interrupted");
        }
        System.out.printf("%s finished... \n", Thread.currentThread().getName());
    }
}
