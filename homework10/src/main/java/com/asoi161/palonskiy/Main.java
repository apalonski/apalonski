package com.asoi161.palonskiy;

public class Main {
    public static void main(String[] args) {
        Card card = new Card();
        MoneyConsumer moneyConsumer = new MoneyConsumer(card);
        MoneyProducer moneyProducer = new MoneyProducer(card);
        int i = 0;
        while (i < 3) {
            new Thread(moneyConsumer).start();
            new Thread(moneyProducer).start();
            i++;
        }
    }
}
