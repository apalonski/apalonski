package com.asoi161.palonskiy;

public class MoneyConsumer implements Runnable {
    Card card;

    public MoneyConsumer(Card card) {
        this.card = card;
    }

    @Override
    public void run() {
        System.out.printf("%s started... \n", Thread.currentThread().getName());
        try {
            while (card.getBalance() > 0) {
                card.withdraw(5);
                Thread.sleep(2000);
            }
        } catch (InterruptedException e) {
            System.out.println("Thread has been interrupted");
        }
        System.out.printf("%s finished... \n", Thread.currentThread().getName());
    }
}
