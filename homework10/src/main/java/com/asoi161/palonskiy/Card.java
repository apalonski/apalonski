package com.asoi161.palonskiy;

public class Card {
    private int balance = 500;

    public synchronized int getBalance() {
        return balance;
    }

    public void withdraw(int bal) {
        synchronized( this ) {
            balance = balance - bal;
            System.out.println("Balance after withdraw = " + balance);
        }
    }

    public void deposit(int bal) {
        synchronized( this ) {
            balance = balance + bal;
            System.out.println("Balance after deposit = " + balance);
        }
    }
}
