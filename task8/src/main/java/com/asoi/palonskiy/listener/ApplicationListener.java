package com.asoi.palonskiy.listener;

import com.asoi.palonskiy.dbutil.BdCreator;
import com.asoi.palonskiy.dbutil.BdFiller;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

@WebListener
public class ApplicationListener implements ServletContextListener {
    private static final String JDBC_DRIVER = "org.h2.Driver";
    private static final Logger LOGGER = LoggerFactory.getLogger(ApplicationListener.class);

    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {

        try {
            Class.forName(JDBC_DRIVER);
            ApplicationContext applicationContext = new ClassPathXmlApplicationContext("conf.xml");
            servletContextEvent.getServletContext().setAttribute("applicationContext", applicationContext);
            BdCreator.createBd();
            BdFiller.fillDb();
        } catch (ClassNotFoundException e) {
            LOGGER.error(e.getMessage());
        }
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {

    }
}
