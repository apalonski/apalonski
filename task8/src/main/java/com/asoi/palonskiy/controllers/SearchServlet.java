package com.asoi.palonskiy.controllers;

import com.asoi.palonskiy.model.Book;
import com.asoi.palonskiy.service.BookService;
import org.springframework.context.ApplicationContext;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

@WebServlet("/books/search")
public class SearchServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String name = req.getParameter("name");
        HttpSession session = req.getSession();
        ServletContext servletContext = session.getServletContext();
        ApplicationContext applicationContext = (ApplicationContext) servletContext.getAttribute("applicationContext");
        BookService bookService = (BookService) applicationContext.getBean("bookServiceImpl");
        List<Book> correct = bookService.search(name);
        req.setAttribute("correct", correct);
        check(correct, req);
        req.getRequestDispatcher("/searchResult.jsp").forward(req, resp);
    }

    public void check(List<Book> list, HttpServletRequest req) {
        if (list.isEmpty()) {
            req.setAttribute("correct", "The book wasn't found.");
        }
    }
}
