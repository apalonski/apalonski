package com.asoi.palonskiy.dbutil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

public class BdCreator {

    private static final String DB_URL = "jdbc:h2:mem:test;DB_CLOSE_DELAY=-1";
    private static final String USER = "sa";
    private static final String PASS = "";
    private static final Logger LOGGER = LoggerFactory.getLogger(BdCreator.class);
    private BdCreator(){

    }

    public static void createBd() {
        try (Connection conn = DriverManager.getConnection(DB_URL, USER, PASS) ; Statement stmt = conn.createStatement();) {
            String sql = "CREATE TABLE   IN_MEM_BOOK " +
                    "(id INTEGER not NULL, " +
                    " name VARCHAR(255), " +
                    " author VARCHAR(255), " +
                    " date date , " +
                    " publisher VARCHAR(255), " +
                    " PRIMARY KEY ( id ))";
            stmt.executeUpdate(sql);
            sql = "CREATE TABLE   USERS " +
                    "(login VARCHAR(255), " +
                    " password VARCHAR(255), " +
                    " PRIMARY KEY ( login ))";
            stmt.executeUpdate(sql);
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }
    }
}
