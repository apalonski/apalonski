package com.asoi.palonskiy.dbutil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

public class BdFiller {

    private static final String INSERT_LINE = "INSERT INTO IN_MEM_BOOK ";
    private static final Logger LOGGER = LoggerFactory.getLogger(BdFiller.class);
    private static final String DB_URL = "jdbc:h2:mem:test;DB_CLOSE_DELAY=-1";
    private static final String USER = "sa";
    private static final String PASS = "";

    private BdFiller(){

    }
    public static void fillDb() {

        try (Connection conn = DriverManager.getConnection(DB_URL, USER, PASS); Statement stmt = conn.createStatement();) {
            String sql = INSERT_LINE + "VALUES (1, 'Book1','author1', TO_DATE('2001-01-01', 'yyyy-mm-dd') ,'publisher1')";
            stmt.executeUpdate(sql);
            sql = INSERT_LINE + "VALUES (2, 'Book2','author2',  TO_DATE('2002-02-02', 'yyyy-mm-dd'), 'publisher2')";
            stmt.executeUpdate(sql);
            sql = INSERT_LINE + "VALUES (3, 'Book3','author3',  TO_DATE('2003-03-03', 'yyyy-mm-dd'), 'publisher3')";
            stmt.executeUpdate(sql);
            sql = INSERT_LINE + "VALUES (4, 'Book4','author4',  TO_DATE('2004-04-04', 'yyyy-mm-dd'), 'publisher4')";
            stmt.executeUpdate(sql);
            sql = "INSERT INTO USERS " + "VALUES ('user1', 'user1')";
            stmt.executeUpdate(sql);
            sql = "INSERT INTO USERS " + "VALUES ('user2', 'user1')";
            stmt.executeUpdate(sql);
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }
    }
}
