package com.asoi.palonskiy.dao;

import com.asoi.palonskiy.model.Book;

import java.util.List;

public interface BookDao {
    List<Book> getData();

    void insert(Book book);

    long getLastId();

    void update(Book book);
}
