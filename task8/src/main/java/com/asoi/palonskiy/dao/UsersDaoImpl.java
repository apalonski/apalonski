package com.asoi.palonskiy.dao;

import com.asoi.palonskiy.model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class UsersDaoImpl implements UsersDao {

    private static final Logger LOGGER = LoggerFactory.getLogger(BookDaoImpl.class);
    private DataSource dataSource;

    public UsersDaoImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public List<User> getUser() {
        List<User> users = new ArrayList<>();
        String sql = "SELECT login, password FROM USERS";
        try (Connection conn = dataSource.getConnection(); Statement stmt = conn.createStatement(); ResultSet rs = stmt.executeQuery(sql);) {
            while (rs.next()) {
                String user = rs.getString("login");
                String password = rs.getString("password");
                users.add(new User(user, password));
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }
        return users;
    }

}
