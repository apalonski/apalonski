package com.asoi161.palonsky.second;

import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.*;

public class UtilClassTest {
    Set<Integer> firstInt;
    Set<Integer> secondInt;
    Set<String> firstString;
    Set<String> secondString;

    @Before
    public void initialize() {
        firstInt = new HashSet<>(Arrays.asList(1, 2, 3, 4));
        secondInt = new HashSet<>(Arrays.asList(3, 4, 5, 6));
        firstString = new HashSet<>(Arrays.asList("1", "2", "3", "4"));
        secondString = new HashSet<>(Arrays.asList("3", "4", "5", "6"));
    }

    @Test
    public void testUnion() {
        Set<Integer> expectedInt = new HashSet<>(Arrays.asList(1, 2, 3, 4, 5, 6));
        Set<Integer> actualInt = UtilClass.union(firstInt, secondInt);
        assertEquals(expectedInt, actualInt);

        Set<String> expectedString = new HashSet<>(Arrays.asList("1", "2", "3", "4", "5", "6"));
        Set<String> actualString = UtilClass.union(firstString, secondString);
        assertEquals(expectedString, actualString);

    }

    @Test
    public void testIntersection() {
        Set<Integer> expectedInt = new HashSet<>(Arrays.asList(3, 4));
        Set<Integer> actualInt = UtilClass.intersection(firstInt, secondInt);
        assertEquals(expectedInt, actualInt);

        Set<String> expectedString = new HashSet<>(Arrays.asList("3", "4"));
        Set<String> actualString = UtilClass.intersection(firstString, secondString);
        assertEquals(expectedString, actualString);
    }

    @Test
    public void testDifference() {
        Set<Integer> expectedInt = new HashSet<>(Arrays.asList(1, 2));
        Set<Integer> actualInt = UtilClass.difference(firstInt, secondInt);
        assertEquals(expectedInt, actualInt);

        Set<String> expectedString = new HashSet<>(Arrays.asList("1", "2"));
        Set<String> actualString = UtilClass.difference(firstString, secondString);
        assertEquals(expectedString, actualString);
    }

    @Test
    public void testSumDifference() {
        Set<Integer> expectedInt = new HashSet<>(Arrays.asList(1, 2, 5, 6));
        Set<Integer> actualInt = UtilClass.sumDifference(firstInt, secondInt);
        assertEquals(expectedInt, actualInt);

        Set<String> expectedString = new HashSet<>(Arrays.asList("1", "2", "5", "6"));
        Set<String> actualString = UtilClass.sumDifference(firstString, firstString);
        assertEquals(expectedString, actualString);
    }
}