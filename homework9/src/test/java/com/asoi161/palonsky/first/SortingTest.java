package com.asoi161.palonsky.first;

import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.PrintStream;

import static org.junit.Assert.*;

public class SortingTest {
    Sorting sorting;
    Integer[] mas;
    Integer[] emptyMas;
    private ByteArrayOutputStream output = new ByteArrayOutputStream();

    @Before
    public void initialize() {
        mas = new Integer[]{11, 48, 35, 144, 99, 1111, 8};
        emptyMas = new Integer[]{};
        System.setOut(new PrintStream(output));
    }

    @Test
    public void testGetSum() {
        assertEquals(13, sorting.getSum(76));
        assertEquals(1, sorting.getSum(1));
        assertEquals(0, sorting.getSum(0));
    }

    @Test
    public void testConstructor() {
        sorting = new Sorting(mas);
        assertEquals("[11, 1111, 35, 8, 144, 48, 99]", output.toString());
    }

    @Test
    public void testNegativeConstructor() {
        sorting = new Sorting(emptyMas);
        assertEquals("[]", output.toString());
    }
}