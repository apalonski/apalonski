package com.asoi161.palonsky.second;

import java.util.HashSet;
import java.util.Set;

public class UtilClass {
    private UtilClass() {

    }

    public static <T> Set<T> union(Set<T> first, Set<T> second) {
        Set<T> result = new HashSet<>(first);
        result.addAll(second);
        return result;
    }

    public static <T> Set<T> intersection(Set<T> first, Set<T> second) {
        Set<T> result = new HashSet<>(second);
        result.retainAll(first);
        return result;
    }

    public static <T> Set<T> difference(Set<T> first, Set<T> second) {
        Set<T> result = new HashSet<>(first);
        result.removeAll(second);
        return result;
    }

    public static <T> Set<T> sumDifference(Set<T> first, Set<T> second) {
        Set<T> union = union(first, second);
        Set<T> intersection = intersection(first, second);
        Set<T> result = difference(union, intersection);
        return result;
    }
}
