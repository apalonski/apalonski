package com.asoi161.palonsky.first;

import java.util.Arrays;
import java.util.Comparator;

public class Sorting {
    public Sorting(Integer[] mas) {
        Integer[] masInt = mas;
        Comparator<Integer> comparator = new Comparator<Integer>() {
            @Override
            public int compare(Integer first, Integer second) {
                if (getSum(first) == getSum(second)) {
                    return 0;
                } else if (getSum(first) < getSum(second)) {
                    return -1;
                } else {
                    return 1;
                }
            }
        };
        Arrays.sort(masInt, comparator);
        System.out.print(Arrays.toString(masInt));
    }

    public static int getSum(int numb) {
        int sum = 0;
        for (int i = numb; i != 0; i /= 10) {
            sum += (i % 10);
        }
        return sum;
    }
}
