package com.asoi161.palonsky.second.brigade;

import com.asoi161.palonsky.second.worker.Worker;

import java.util.ArrayList;

public class Brigade {
    private ArrayList<Worker> masOfWorkers;
    private int price;
    private String name;

    public Brigade(ArrayList<Worker> masOfWorkers, int price, String name) {
        this.masOfWorkers = masOfWorkers;
        this.price = price;
        this.name = name;
    }

    public ArrayList<Worker> getWorkers() {
        return masOfWorkers;
    }

    public int getPrice() {
        return price;
    }

    public String toString() {
        return name;
    }
}
