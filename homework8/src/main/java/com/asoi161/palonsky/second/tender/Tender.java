package com.asoi161.palonsky.second.tender;

import com.asoi161.palonsky.second.worker.Worker;
import com.asoi161.palonsky.second.brigade.Brigade;

import java.util.*;

public class Tender {
    private ArrayList<Brigade> masOfBrigades;
    private ArrayList<Worker.Skills> masOfSkills;


    public Tender(ArrayList<Brigade> masOfBrigades, ArrayList<Worker.Skills> masOfSkills) {
        this.masOfBrigades = masOfBrigades;
        this.masOfSkills = masOfSkills;
    }

    public Brigade getWinner() {
        Brigade winner = null;
        Collections.sort(masOfSkills);
        for (Brigade brigade : masOfBrigades) {
            Set<Worker.Skills> setOfSkills = new LinkedHashSet<>();
            for (Worker worker : brigade.getWorkers()) {
                ArrayList<Worker.Skills> workerSkills = worker.getSkills();
                for (int i = 0; i < workerSkills.size(); i++) {
                    if (masOfSkills.contains(workerSkills.get(i))){
                        setOfSkills.add(workerSkills.get(i));
                    }
                }
            }
            Arrays.sort(setOfSkills.toArray());
            if (setOfSkills.size()==masOfSkills.size()){
                if (winner!=null){
                    if (brigade.getPrice()<winner.getPrice()){
                        winner = brigade;
                    }
                }
                else {
                    winner = brigade;
                }


            }
        }
        return winner;
    }
}
