package com.asoi161.palonsky.second.client;

import com.asoi161.palonsky.second.brigade.Brigade;
import com.asoi161.palonsky.second.tender.Tender;
import com.asoi161.palonsky.second.worker.Worker;

import java.util.ArrayList;
import java.util.Arrays;

public class Client {
    public static void main(String[] args) {
        ArrayList<Worker> masOfWorker1 = new ArrayList<>(Arrays.asList(
                new Worker(new ArrayList<>(Arrays.asList(Worker.Skills.BUILDER))) ,
                new Worker(new ArrayList<>(Arrays.asList(Worker.Skills.TALL, Worker.Skills.WELDER))),
                new Worker(new ArrayList<>(Arrays.asList(Worker.Skills.BUILDER, Worker.Skills.SKILLED, Worker.Skills.STRONG)))
        ));
        Brigade brigade3 = new Brigade(masOfWorker1,10,"a");


        ArrayList<Worker> masOfWorker2 = new ArrayList<>(Arrays.asList(
                new Worker(new ArrayList<>(Arrays.asList(Worker.Skills.WELDER, Worker.Skills.SKILLED, Worker.Skills.SMART))) ,
                new Worker(new ArrayList<>(Arrays.asList(Worker.Skills.TALL))),
                new Worker(new ArrayList<>(Arrays.asList(Worker.Skills.WELDER)))
        ));
        Brigade brigade2 = new Brigade(masOfWorker2,9,"b");


        ArrayList<Worker> masOfWorker3 = new ArrayList<>(Arrays.asList(
                new Worker(new ArrayList<>(Arrays.asList(Worker.Skills.WELDER, Worker.Skills.STRONG, Worker.Skills.SMART))) ,
                new Worker(new ArrayList<>(Arrays.asList(Worker.Skills.TALL))),
                new Worker(new ArrayList<>(Arrays.asList(Worker.Skills.WELDER)))
        ));
        Brigade brigade1 = new Brigade(masOfWorker3,9,"c");


        ArrayList<Worker> masOfWorker4 = new ArrayList<>(Arrays.asList(
                new Worker(new ArrayList<>(Arrays.asList(Worker.Skills.WELDER, Worker.Skills.STRONG, Worker.Skills.SMART))) ,
                new Worker(new ArrayList<>(Arrays.asList(Worker.Skills.TALL))),
                new Worker(new ArrayList<>(Arrays.asList(Worker.Skills.WELDER)))
        ));
        Brigade brigade4 = new Brigade(masOfWorker4,9,"d");


        Tender tender = new Tender(
                new ArrayList<>(Arrays.asList(brigade1,brigade2,brigade3, brigade4)),
                new ArrayList<>(Arrays.asList(Worker.Skills.SMART, Worker.Skills.STRONG, Worker.Skills.TALL))
                );
        Brigade winner = tender.getWinner();
        System.out.println(winner);
    }
}
