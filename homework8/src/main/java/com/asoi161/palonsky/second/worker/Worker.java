package com.asoi161.palonsky.second.worker;

import java.util.ArrayList;

public class Worker {
    public enum Skills {
        STRONG,
        SMART,
        TALL,
        SKILLED,
        WELDER,
        BUILDER
    }
    private ArrayList<Skills> masOfSkills;
    public Worker(ArrayList<Skills> masOfSkills){
        this.masOfSkills = masOfSkills;
    }
    public ArrayList<Skills> getSkills(){
        return masOfSkills;
    }
}
