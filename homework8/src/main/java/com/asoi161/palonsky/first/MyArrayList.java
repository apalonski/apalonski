package com.asoi161.palonsky.first;

import java.util.*;

public class MyArrayList<E> implements List<E> {
    private Object[] masOfElements;
    private int size;
    private int index;

    private static final int DEFAULT_CAPACITY = 10;

    public MyArrayList() {
        masOfElements = new Object[DEFAULT_CAPACITY];
    }

    public MyArrayList(int capacity) {
        checkSize(capacity);
        masOfElements = new Object[capacity];
    }

    public MyArrayList(Collection<? extends E> c) {
        checkSize(c.size());
        masOfElements = c.toArray();
        size=c.size();
        index=c.size();
    }

    public boolean add(Object value) {
        checkSize(size+1);
        if (index == masOfElements.length)
            extendArray();
        masOfElements[index] = value;
        index++;
        size++;
        return true;
    }

    public void add(int index, E value) {
        checkIndex(index);
        checkSize(size+1);
        System.arraycopy(masOfElements, index, masOfElements, index + 1, this.index - index);
        masOfElements[index] = value;
        this.index++;
        size++;
    }
    public boolean addAll(Collection c) {
        if (c == null) {
            return false;
        }
        if (c.isEmpty()) {
            return false;
        }
        for (Object item : c) {
            add(item);
        }
        return true;
    }

    public E get(int index) {
        checkIndex(index);
        return (E) masOfElements[index];
    }

    public E set(int index, Object value) {
        checkIndex(index);
        masOfElements[index] = value;
        return (E)masOfElements[index];
    }

    public int size() {
        return size;
    }

    public int indexOf(Object value) {
        int result = -1;
        for (int i = 0; i < index; i++) {
            if (masOfElements[i] == value) {
                result = i;
                break;
            }
        }
        return result;
    }

    public E remove(int index) {
        checkIndex(index);
        E oldElement = (E) masOfElements[index];
        System.arraycopy(masOfElements, index + 1, masOfElements, index, this.index - index);
        size--;
        this.index--;
        return oldElement;
    }

    public boolean contains(Object o) {
        return indexOf(o) >= 0;
    }

    private void extendArray() {
        Object[] newArray = new Object[masOfElements.length * 2];
        System.arraycopy(masOfElements, 0, newArray, 0, index - 1);
        masOfElements = newArray;
    }


    private void checkIndex(int index) {
        if (index < 0 || index >= this.index)
            throw new ListIndexOutOfBoundsException("Index out of legal bounds",index);
    }
    private void checkSize(int size) {
        if (size>DEFAULT_CAPACITY)
            throw new ListIndexOutOfBoundsException("Index out of legal bounds",index);
    }


    @Override
    public boolean isEmpty() {
        return false;
    }


    @Override
    public Iterator<E> iterator() {
        return null;
    }

    @Override
    public Object[] toArray() {
        return new Object[0];
    }

    @Override
    public <T> T[] toArray(T[] a) {
        return null;
    }


    @Override
    public boolean containsAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean addAll(int index, Collection<? extends E> c) {
        return false;
    }
    @Override
    public boolean remove(Object o){
        return false;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        return false;
    }

    @Override
    public void clear() {

    }


    @Override
    public int lastIndexOf(Object o) {
        return 0;
    }

    @Override
    public ListIterator<E> listIterator() {
        return null;
    }

    @Override
    public ListIterator<E> listIterator(int index) {
        return null;
    }

    @Override
    public List<E> subList(int fromIndex, int toIndex) {
        return null;
    }
}
