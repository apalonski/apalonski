package com.asoi161.palonsky.first;

public class ListIndexOutOfBoundsException extends ArrayIndexOutOfBoundsException {
    private Object number;
    public Object getObject(){return number;}
    public <T>ListIndexOutOfBoundsException(String message, T object){

        super(message);
        this.number=object;
    }
}
