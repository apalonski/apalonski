package com.asoi161.palonsky.second.brigade;

import com.asoi161.palonsky.second.worker.Worker;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.Assert.*;

public class BrigadeTest {
    ArrayList<Worker> masOfWorker;
    Brigade brigade2;
    @Before
    public  void inic(){
        masOfWorker = new ArrayList<>(Arrays.asList(
                new Worker(new ArrayList<>(Arrays.asList(Worker.Skills.WELDER, Worker.Skills.SKILLED, Worker.Skills.SMART))) ,
                new Worker(new ArrayList<>(Arrays.asList(Worker.Skills.TALL))),
                new Worker(new ArrayList<>(Arrays.asList(Worker.Skills.WELDER)))
        ));
        brigade2 = new Brigade(masOfWorker,9,"b");
    }

    @Test
    public void testGetWorkers() {
        assertEquals(brigade2.getWorkers(), masOfWorker);
    }

    @Test
    public void testGetPrice() {
        assertEquals(brigade2.getPrice(), 9);
    }

    @Test
    public void testToString() {
        assertEquals(brigade2.toString(), "b");
    }
}