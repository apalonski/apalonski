package com.asoi161.palonsky.second.worker;

import static org.junit.Assert.*;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;


public class WorkerTest {

    @Test
    public void testGetSkills() {
        ArrayList<Worker.Skills> skills = new ArrayList<>(Arrays.asList(Worker.Skills.BUILDER, Worker.Skills.SKILLED, Worker.Skills.STRONG));
        Worker worker = new Worker(new ArrayList<>(skills));
        assertEquals(skills.toArray(),worker.getSkills().toArray());

    }
}