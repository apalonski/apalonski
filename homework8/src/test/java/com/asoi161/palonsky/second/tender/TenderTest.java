package com.asoi161.palonsky.second.tender;

import com.asoi161.palonsky.second.brigade.Brigade;
import com.asoi161.palonsky.second.worker.Worker;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.Assert.*;

public class TenderTest {
    Tender negativeTender;
    Tender tender;
    Brigade brigade3;
    @Before
    public void inic(){
        ArrayList<Worker> masOfWorker3 = new ArrayList<>(Arrays.asList(
            new Worker(new ArrayList<>(Arrays.asList(Worker.Skills.WELDER, Worker.Skills.STRONG, Worker.Skills.SMART))) ,
            new Worker(new ArrayList<>(Arrays.asList(Worker.Skills.SKILLED))),
            new Worker(new ArrayList<>(Arrays.asList(Worker.Skills.WELDER)))
        ));
        Brigade brigade1 = new Brigade(masOfWorker3,11,"a");


        ArrayList<Worker> masOfWorker2 = new ArrayList<>(Arrays.asList(
                new Worker(new ArrayList<>(Arrays.asList(Worker.Skills.WELDER, Worker.Skills.SKILLED, Worker.Skills.SMART))) ,
                new Worker(new ArrayList<>(Arrays.asList(Worker.Skills.SKILLED, Worker.Skills.BUILDER))),
                new Worker(new ArrayList<>(Arrays.asList(Worker.Skills.WELDER)))
        ));
        Brigade brigade2 = new Brigade(masOfWorker2,9,"b");


        ArrayList<Worker> masOfWorker1 = new ArrayList<>(Arrays.asList(
                new Worker(new ArrayList<>(Arrays.asList(Worker.Skills.BUILDER, Worker.Skills.SMART))) ,
                new Worker(new ArrayList<>(Arrays.asList(Worker.Skills.TALL, Worker.Skills.WELDER))),
                new Worker(new ArrayList<>(Arrays.asList(Worker.Skills.BUILDER, Worker.Skills.SKILLED, Worker.Skills.STRONG)))
        ));
        brigade3 = new Brigade(masOfWorker1,7,"c");


       ArrayList<Worker> masOfWorker4 = new ArrayList<>(Arrays.asList(
                new Worker(new ArrayList<>(Arrays.asList(Worker.Skills.WELDER, Worker.Skills.STRONG, Worker.Skills.SMART))) ,
                new Worker(new ArrayList<>(Arrays.asList(Worker.Skills.TALL))),
                new Worker(new ArrayList<>(Arrays.asList(Worker.Skills.WELDER)))
        ));
        Brigade brigade4 = new Brigade(masOfWorker4,8,"d");


        negativeTender = new Tender(
                new ArrayList<>(Arrays.asList(brigade1,brigade2)),
                new ArrayList<>(Arrays.asList(Worker.Skills.SMART, Worker.Skills.STRONG, Worker.Skills.TALL))
        );
        tender = new Tender(
                new ArrayList<>(Arrays.asList(brigade1,brigade2,brigade3,brigade4)),
                new ArrayList<>(Arrays.asList(Worker.Skills.SMART, Worker.Skills.STRONG, Worker.Skills.TALL))
        );
    }

    @Test
    public void testGetWinnerNegative() {
        assertEquals(negativeTender.getWinner(),null);
    }
    @Test
    public void testGetWinner() {
        assertEquals(tender.getWinner(),brigade3);
    }
}