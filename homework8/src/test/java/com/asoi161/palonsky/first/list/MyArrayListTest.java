package com.asoi161.palonsky.first.list;

import com.asoi161.palonsky.first.ListIndexOutOfBoundsException;
import com.asoi161.palonsky.first.MyArrayList;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

public class MyArrayListTest {
    List<String> mainNegativeList = new MyArrayList<>();
    List<String> mainList = new ArrayList<>();

    @Before
    public void init() {
        mainNegativeList.addAll(Arrays.asList("a1","a2","a3","a4","a5","a6","a7","a8","a9","a10"));
        mainList.addAll(Arrays.asList("a","b","c"));
    }

    @Test(expected = ListIndexOutOfBoundsException.class)
    public void testCollectionConstNegative() {
        Integer[] mas = new Integer[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11};
        List<Integer> list = new MyArrayList<>(Arrays.asList(mas));
    }

    @Test(expected = ListIndexOutOfBoundsException.class)
    public void testCapacityConstNegative() {
        List<Integer> list = new MyArrayList<>(11);
    }

    @Test(expected = ListIndexOutOfBoundsException.class)
    public void testAddWithoutIndexNegative() {
        mainNegativeList.add("a11");
    }


    @Test(expected = ListIndexOutOfBoundsException.class)
    public void testAddWithIndexNegative() {
        mainNegativeList.add(1, "a11");
    }

    @Test(expected = ListIndexOutOfBoundsException.class)
    public void testAddWithIndexSimpleNegative() {
        List<Integer> list = new MyArrayList<>(2);
        list.add(2, 2);
    }

    @Test(expected = ListIndexOutOfBoundsException.class)
    public void testRemoveNegative() {
        mainNegativeList.remove(10);
    }

    @Test(expected = ListIndexOutOfBoundsException.class)
    public void testGetNegative() {
        mainNegativeList.get(10);
    }

    @Test(expected = ListIndexOutOfBoundsException.class)
    public void testSetNegative() {
        mainNegativeList.set(11, "a");
    }

    @Test
    public void testIndexOf() {
        int negativeExpect = -1;
        int expect = 1;
        assertEquals(mainNegativeList.indexOf("a2"), expect);
        assertEquals(mainNegativeList.indexOf("a11"), negativeExpect);
    }

    @Test
    public void testContains() {
        boolean negativeExpect=false;
        boolean expect = true;
        assertEquals(mainNegativeList.contains("a11"),negativeExpect );
        assertEquals(mainNegativeList.contains("a2"),expect );
    }
}