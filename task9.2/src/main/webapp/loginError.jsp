<%--
  Created by IntelliJ IDEA.
  User: Sasha
  Date: 03.07.2019
  Time: 22:00
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Ошибка 404</title>
</head>
<body>
<img src="images/404-html.png" alt="Ошибка 404">
<p>К сожалению, запрашиваемая Вами страница не найдена..</p>
<p>Почему?</p>
<div>Попытка получения доступа к защищенному контенту<br>
    без прав на доступ.
</div>


</body>
</html>
