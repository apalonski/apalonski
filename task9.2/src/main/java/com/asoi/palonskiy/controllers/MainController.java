package com.asoi.palonskiy.controllers;


import com.asoi.palonskiy.exception.MainServletExceptoin;
import com.asoi.palonskiy.model.Book;
import com.asoi.palonskiy.service.BookService;
import com.asoi.palonskiy.service.BookServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;


@Controller
@RequestMapping("/books")
public class MainController{
    private BookService bookService;
    private static final Logger LOGGER = LoggerFactory.getLogger(BookServiceImpl.class);

    public MainController(BookService bookService){
        this.bookService =bookService;
    }

    @RequestMapping(method = RequestMethod.GET)
    public String doMain(HttpServletRequest req){
        try {
            HttpSession session = req.getSession();
            List<Book> list = bookService.getList();
            if (req.getParameter("ListOfBooks") != null) {
                req.setAttribute("books", list);
                return "list";
            } else if (req.getParameter("Search") != null) {
                return "search";
            } else if (req.getParameter("LogOut") != null) {
                session.invalidate();
                req.setAttribute("lable", "Successfully logged out");
                return "index";
            } else if (req.getParameter("Update/Add") != null) {
                if (req.getParameter("id").equals("")||!req.getParameter("id").equals("new")) {
                    throw new MainServletExceptoin();
                } else if (req.getParameter("id").equals("new")) {
                    return "add";
                } else if (Integer.parseInt(req.getParameter("id")) != 0 && (Integer.parseInt(req.getParameter("id")) <= list.size())) {
                    session.setAttribute("id", req.getParameter("id"));
                    return "update";
                } else throw new MainServletExceptoin();
            }
        }catch (MainServletExceptoin ex){
            return "error";
        }
        return "";
    }

}
