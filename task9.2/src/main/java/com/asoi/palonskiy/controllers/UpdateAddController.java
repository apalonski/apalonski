package com.asoi.palonskiy.controllers;

import com.asoi.palonskiy.model.Book;
import com.asoi.palonskiy.service.BookService;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

@Controller
@RequestMapping("/books/updateAdd")
public class UpdateAddController{
    private static final String BOOKS = "books";

    private BookService bookService;

    public UpdateAddController(BookService bookService) {
        this.bookService = bookService;
    }

    @RequestMapping(method = RequestMethod.GET)
    public String doAdd(HttpServletRequest req){
        long id = (long) bookService.getList().size() + 1;
        bookService.insert(id, req);
        List<Book> list = bookService.getList();
        HttpSession session = req.getSession();
        session.setAttribute(BOOKS, list);
        return "list";
    }

    @RequestMapping(method = RequestMethod.POST)
    public String doUpdate(HttpServletRequest req){
        HttpSession session = req.getSession();
        long id = Long.parseLong((session.getAttribute("id")).toString());
        bookService.update(id, req);
        List<Book> list = bookService.getList();
        session.setAttribute(BOOKS, list);
        return "list";
    }



}
