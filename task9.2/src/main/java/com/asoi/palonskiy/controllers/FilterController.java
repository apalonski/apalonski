package com.asoi.palonskiy.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


@Controller
@RequestMapping("/filterServlet")
public class FilterController {


    @RequestMapping(method = RequestMethod.POST)
    public String getMain() {
        return "main";
    }
}
