package com.asoi.palonskiy.service;

import com.asoi.palonskiy.dao.UsersDao;
import com.asoi.palonskiy.model.User;
import org.springframework.stereotype.Service;


import java.util.List;

@Service
public class UserServiceImpl implements UserService {
    private UsersDao usersDao;

    public UserServiceImpl(UsersDao usersDao) {
        this.usersDao = usersDao;
    }


    public boolean checkUser(String name) {
        List<User> users = usersDao.getUser();
        return users.stream()
                .anyMatch(user -> user.getName().equals(name));

    }

    public boolean checkPassword(String name) {
        List<User> users = usersDao.getUser();
        return users.stream()
                .anyMatch(user -> user.getPassword().equals(name));
    }
}
