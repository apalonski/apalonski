package com.asoi.palonskiy.dao;

import com.asoi.palonskiy.model.User;

import java.util.List;

public interface UsersDao {
    List<User> getUser();
}
