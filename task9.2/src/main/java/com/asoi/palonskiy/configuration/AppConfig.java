package com.asoi.palonskiy.configuration;

import com.asoi.palonskiy.connection.BdConnection;
import com.asoi.palonskiy.dao.BookDao;
import com.asoi.palonskiy.dao.BookDaoImpl;
import com.asoi.palonskiy.dao.UsersDao;
import com.asoi.palonskiy.dao.UsersDaoImpl;
import com.asoi.palonskiy.service.BookServiceImpl;
import com.asoi.palonskiy.service.UserServiceImpl;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;


@Configuration
@EnableWebMvc
@ComponentScan("com.asoi.palonskiy")
@PropertySource("classpath:bd/bd.properties")
public class AppConfig implements WebMvcConfigurer {

    @Value("${db.url}")
    private String url;
    @Value("${db.login}")
    private String login;
    @Value("${db.password}")
    private String password;


    @Bean
    public BdConnection getConnection() {
        return new BdConnection(url, login, password);
    }

    @Bean
    public ViewResolver viewResolver() {
        InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
        viewResolver.setViewClass(JstlView.class);
        viewResolver.setPrefix("/");
        viewResolver.setSuffix(".jsp");
        return viewResolver;
    }
    public void addResourceHandlers(final ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/resources/**").addResourceLocations("/resources/");
    }
}
