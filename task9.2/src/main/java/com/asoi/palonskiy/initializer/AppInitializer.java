package com.asoi.palonskiy.initializer;

import com.asoi.palonskiy.configuration.AppConfig;
import com.asoi.palonskiy.filter.UserFilter;
import com.asoi.palonskiy.model.User;
import com.asoi.palonskiy.service.UserService;
import org.springframework.web.filter.DelegatingFilterProxy;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

import javax.servlet.Filter;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;

public class AppInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {
    @Override
    protected Class<?>[] getRootConfigClasses() {
        return new Class[] { AppConfig.class };
    }

    @Override
    protected Class<?>[] getServletConfigClasses() {
        return new Class[]{};
    }

    @Override
    protected String[] getServletMappings() {
        return new String[] { "/" };
    }

    @Override
    public void onStartup(ServletContext servletContext) throws ServletException {
        servletContext.addFilter("filter", new
                DelegatingFilterProxy("filter"))
                .addMappingForUrlPatterns(null, false, "/filterServlet", "/books/*");
        super.onStartup(servletContext);
    }

}
