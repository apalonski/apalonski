package com.asoi.palonskiy.dbutil;

import com.asoi.palonskiy.connection.BdConnection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;


import javax.annotation.PostConstruct;
import java.sql.Statement;

@Component
public class BdCreatorAndFiller {

    private static final Logger LOGGER = LoggerFactory.getLogger(BdCreatorAndFiller.class);
    private static final String INSERT_LINE = "INSERT INTO IN_MEM_BOOK ";
    private static BdConnection bdConnection;

    public BdCreatorAndFiller(BdConnection bdConnection) {
        this.bdConnection = bdConnection;
    }

    @PostConstruct
    public static void getDriver() {
        try {
            Class.forName("org.h2.Driver");
            createBd();
            fillDb();
        } catch (ClassNotFoundException e) {
            LOGGER.error(e.getMessage());
        }
    }

    public static void createBd() {
        try (Statement stmt = bdConnection.getConnection().createStatement();) {
            String sql = "CREATE TABLE   IN_MEM_BOOK " +
                    "(id INTEGER not NULL, " +
                    " name VARCHAR(255), " +
                    " author VARCHAR(255), " +
                    " date date , " +
                    " publisher VARCHAR(255), " +
                    " PRIMARY KEY ( id ))";
            stmt.executeUpdate(sql);
            sql = "CREATE TABLE   USERS " +
                    "(login VARCHAR(255), " +
                    " password VARCHAR(255), " +
                    " PRIMARY KEY ( login ))";
            stmt.executeUpdate(sql);
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }
    }

    public static void fillDb() {
        try ( Statement stmt = bdConnection.getConnection().createStatement();) {
            String sql = INSERT_LINE + "VALUES (1, 'Book1','author1', TO_DATE('2001-01-01', 'yyyy-mm-dd') ,'publisher1')";
            stmt.executeUpdate(sql);
            sql = INSERT_LINE + "VALUES (2, 'Book2','author2',  TO_DATE('2002-02-02', 'yyyy-mm-dd'), 'publisher2')";
            stmt.executeUpdate(sql);
            sql = INSERT_LINE + "VALUES (3, 'Book3','author3',  TO_DATE('2003-03-03', 'yyyy-mm-dd'), 'publisher3')";
            stmt.executeUpdate(sql);
            sql = INSERT_LINE + "VALUES (4, 'Book4','author4',  TO_DATE('2004-04-04', 'yyyy-mm-dd'), 'publisher4')";
            stmt.executeUpdate(sql);
            sql = "INSERT INTO USERS " + "VALUES ('user1', 'user1')";
            stmt.executeUpdate(sql);
            sql = "INSERT INTO USERS " + "VALUES ('user2', 'user1')";
            stmt.executeUpdate(sql);
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }
    }

}
