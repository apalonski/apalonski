package com.asoi.palonskiy.controllers;

import com.asoi.palonskiy.dao.BookDao;
import com.asoi.palonskiy.dao.BookDaoImpl;
import com.asoi.palonskiy.model.Book;
import org.hibernate.SessionFactory;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet("/books/updateAdd")
public class UpdateAddServlet extends HttpServlet {
    private static final String BOOKS = "books";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        BookDao bookDao = new BookDaoImpl((SessionFactory) req.getSession().getServletContext().getAttribute("factory"));
        long id = bookDao.getData().size();
        bookDao.insert(getBook(id, req));
        HttpSession session = req.getSession();
        session.setAttribute(BOOKS, bookDao.getData());
        req.getRequestDispatcher("/list.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();
        BookDao bookDao = new BookDaoImpl((SessionFactory) req.getSession().getServletContext().getAttribute("factory"));
        long id = Long.parseLong((session.getAttribute("id")).toString());
        bookDao.update(getBook(id, req));
        session.setAttribute(BOOKS, bookDao.getData());
        req.getRequestDispatcher("/list.jsp").forward(req, resp);
    }

    public Book getBook(long id, HttpServletRequest req) {
        return new Book(id, req.getParameter("name"), req.getParameter("date"), req.getParameter("author"),
                req.getParameter("publisher"));

    }


}
