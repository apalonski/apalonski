package com.asoi.palonskiy.listener;


import com.asoi.palonskiy.model.Book;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;


import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

@WebListener
public class ApplicationListener implements ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        Book book1 = new Book(1L, "Book1", "author1", "2001-01-01", "publisher1");
        Book book2 = new Book(2L, "Book2", "author2", "2002-02-02", "publisher2");
        Book book3 = new Book(3L, "Book3", "author3", "2003-03-03", "publisher3");
        Book book4 = new Book(4L, "Book4", "author4", "2004-04-04", "publisher4");

        SessionFactory sessionFactory = new Configuration().configure()
                .buildSessionFactory();
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        session.save(book1);
        session.save(book2);
        session.save(book3);
        session.save(book4);
        session.getTransaction().commit();
        session.close();
        ServletContext context = servletContextEvent.getServletContext();
        context.setAttribute("factory", sessionFactory);
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {

    }
}
