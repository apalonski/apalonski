package com.asoi.palonskiy.model;

import java.util.Objects;


public class Book {
    private String name;
    private String date;
    private String author;
    private String publisher;


    private long id;

    public Book(long id, String name, String date, String author, String publisher) {
        this.author = author;
        this.name = name;
        this.date = date;
        this.publisher = publisher;
        this.id = id;
    }

    public Book() {

    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getAuthor() {
        return author;
    }

    public String getDate() {
        return date;
    }

    public String getName() {
        return name;
    }

    public String getPublisher() {
        return publisher;
    }

    public long getId() {
        return id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Book book = (Book) o;
        return id == book.id &&
                Objects.equals(name, book.name) &&
                Objects.equals(date, book.date) &&
                Objects.equals(author, book.author) &&
                Objects.equals(publisher, book.publisher);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, date, author, publisher, id);
    }

    public String toString() {
        return String.format("%s %s %s by %s published by %s", id, name, date, author, publisher);
    }

}
