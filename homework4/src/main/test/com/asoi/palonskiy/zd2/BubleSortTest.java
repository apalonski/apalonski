package com.asoi.palonskiy.zd2;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BubleSortTest {
    BubleSort bubleSort =new BubleSort();
    SelectionSort selectionSort = new SelectionSort();
    @Test
    void sortBuble() {
        int [] sorted_mas = new int []{1,2,3,4,5,6,7,8,9,10};
        int [] unsorted_mas = new int []{2,5,4,3,6,7,8,1,10,9};
        assertArrayEquals(bubleSort.sort(unsorted_mas),sorted_mas);
    }
    @Test
    void sortSelection() {
        int [] sorted_mas = new int []{1,2,3,4,5,6,7,8,9,10};
        int [] unsorted_mas = new int []{2,5,4,3,6,7,8,1,10,9};
        assertArrayEquals(selectionSort.sort(unsorted_mas),sorted_mas);
    }
    @Test
    void sortBubleNext() {
        int [] sorted_mas = new int []{1,2,2,2,4,4,5,7,8,11};
        int [] unsorted_mas = new int []{2,4,5,7,2,2,4,1,8,11};
        unsorted_mas = bubleSort.sort(unsorted_mas);
        assertArrayEquals(bubleSort.sort(unsorted_mas),sorted_mas);
    }
    @Test
    void sortSelectionNext() {
        int [] sorted_mas = new int []{1,2,2,2,4,4,5,7,8,11};
        int [] unsorted_mas = new int []{2,4,5,7,2,2,4,1,8,11};
        assertArrayEquals(selectionSort.sort(unsorted_mas),sorted_mas);
    }
}