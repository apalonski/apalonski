package com.asoi.palonskiy.zd1;

import com.asoi.palonskiy.zd1.CreditCard;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CreditCardTest {

    @Test
    void decrease() {
        CreditCard crd1 = new CreditCard ("Sanya",300);
        crd1.decrease(500);
        assertEquals(-200,crd1.getbalance());
    }
    @Test
    void decreaseVr() {
        CreditCard crd1 = new CreditCard ("Sanya",300);
        crd1.decrease(-500);
        assertEquals(300,crd1.getbalance());
    }
}