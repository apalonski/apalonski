package com.asoi.palonskiy.zd1;

import com.asoi.palonskiy.zd1.DebitCard;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DebitCardTest {

    @Test
    void decrease() {
        DebitCard crd1 = new DebitCard("Sanya",300);
        crd1.decrease(500);
        assertEquals(300,crd1.getbalance());
    }
    @Test
    void decreaseVr() {
        DebitCard crd1 = new  DebitCard ("Sanya",300);
        crd1.decrease(-500);
        assertEquals(300,crd1.getbalance());
    }
}