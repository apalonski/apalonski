package com.asoi.palonskiy.zd1;

import com.asoi.palonskiy.zd1.CreditCard;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CardTest {
    @Test
    void increase() {
        CreditCard crd1 = new CreditCard("Sanya", 322);
        crd1.increase(100);
        assertEquals(422, crd1.getbalance());
    }

    @Test
    void converting() {
        CreditCard crd1 = new CreditCard("Sanya", 322);
        crd1.converting(2);
        assertEquals(644, crd1.getbalance());
    }

    @Test
    void increaseMin() {
        CreditCard crd1 = new CreditCard("Sanya",322);
        crd1.increase(-100);
        assertEquals(322, crd1.getbalance());
    }

    @Test
    void convertingMin() {
        CreditCard crd1 = new CreditCard("Sanya", 100);
        crd1.converting(-0.5);
        assertEquals(100, crd1.getbalance());
    }

}