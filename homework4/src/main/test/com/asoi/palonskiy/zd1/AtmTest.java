package com.asoi.palonskiy.zd1;

import com.asoi.palonskiy.zd1.Atm;
import com.asoi.palonskiy.zd1.CreditCard;
import com.asoi.palonskiy.zd1.DebitCard;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class AtmTest {
    public Atm atm;
    @Test
    void getnameWithCredit() {
        atm = new Atm(new CreditCard("Sanya",322));
        assertEquals(atm.getname(),"Sanya");
    }
    @Test
    void getnameWithDebet() {
        atm = new Atm(new DebitCard("Sanya",322));
        assertEquals(atm.getname(),"Sanya");
    }
    @Test
    void getbalanceWithCredit() {
        atm = new Atm(new CreditCard("Sanya",322));
        assertEquals(atm.getbalance(),322);
    }
    @Test
    void getbalanceWithDebet() {
        atm = new Atm(new DebitCard("Sanya",322));
        assertEquals(atm.getbalance(),322);
    }

    @Test
    void increaseWithCredet() {
        atm = new Atm(new CreditCard("Sanya",322));
        atm.increase(100);
        assertEquals(atm.getbalance(),422);
    }
    @Test
    void increaseWithDebet() {
        atm = new Atm(new DebitCard("Sanya",322));
        atm.increase(100);
        assertEquals(atm.getbalance(),422);
    }
    @Test
    void increaseWithCredetWrong() {
        atm = new Atm(new CreditCard("Sanya",322));
        atm.increase(-100);
        assertEquals(atm.getbalance(),322);
    }
    @Test
    void increaseWithDebetWrong() {
        atm = new Atm(new DebitCard("Sanya",322));
        atm.increase(-100);
        assertEquals(atm.getbalance(),322);
    }
    @Test
    void decreaseWithCredet() {
        atm = new Atm(new CreditCard("Sanya",322));
        atm.decrease(100);
        assertEquals(atm.getbalance(),222);
    }
    @Test
    void decreaseWithDebet() {
        atm = new Atm(new DebitCard("Sanya",322));
        atm.decrease(100);
        assertEquals(atm.getbalance(),222);
    }
    @Test
    void decreaseWithCredetWrong() {
        atm = new Atm(new CreditCard("Sanya",322));
        atm.decrease(-100);
        assertEquals(atm.getbalance(),322);
    }
    @Test
    void decreaseWithDebetWrong() {
        atm = new Atm(new DebitCard("Sanya",322));
        atm.decrease(-100);
        assertEquals(atm.getbalance(),322);
    }
    @Test
    void decreaseWithCredetMore() {
        atm = new Atm(new CreditCard("Sanya",300));
        atm.decrease(500);
        assertEquals(atm.getbalance(),-200);
    }
    @Test
    void decreaseWithDebetMore() {
        atm = new Atm(new DebitCard("Sanya",300));
        atm.decrease(500);
        assertEquals(atm.getbalance(),300);
    }
}