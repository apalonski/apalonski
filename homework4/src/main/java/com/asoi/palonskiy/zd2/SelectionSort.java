package com.asoi.palonskiy.zd2;

public class SelectionSort implements Sorter {
    @Override
    public int[] sort(int[] numbers) {
        int min, vrem;

        for (int i = 0; i < numbers.length-1; i++){
            min = i;
            for (int j = i+1; j < numbers.length; j++){
                if (numbers[j] < numbers[min])
                    min = j;
            }
            vrem = numbers[min];
            numbers[min] = numbers[i];
            numbers[i] = vrem;
        }
        return numbers;
    }
}
