package com.asoi.palonskiy.zd2;

public interface Sorter {
    public int[] sort (int[] numbers);
}
