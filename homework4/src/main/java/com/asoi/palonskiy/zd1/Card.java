package com.asoi.palonskiy.zd1;

public abstract class Card {
    public String name;
    public double balance;

    public Card(String nam) {
        this(nam, 0);
    }

    public Card(String nam, double bal) {
        name = nam;
        balance = bal;
    }

    public String getname() {
        return name;
    }

    public double getbalance() {
        return balance;
    }

    public void increase(double numb) {
        if (numb >= 0) {
            balance += numb;
        }

    }

    public abstract void decrease(double numb);

    public void converting(double numb) {
        if (numb > 0) {
            balance *= numb;
        }
    }
}

