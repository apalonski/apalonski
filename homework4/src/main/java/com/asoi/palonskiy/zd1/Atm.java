package com.asoi.palonskiy.zd1;

public class Atm {
    private Card card;

    public Atm(Card card){
        this.card = card;
    }

    public String getname(){
        return card.getname();
    }

    public double getbalance(){
        return card.getbalance();
    }

    public void increase(double numb){
        card.increase(numb);
    }
    public void decrease(double numb){
        card.decrease(numb);
    }

}
