package com.asoi.palonskiy.zd1;

import com.asoi.palonskiy.zd1.Card;

public class CreditCard extends Card {
    public CreditCard(String nam, double bal) {
        super(nam, bal);
    }

    @Override
    public void decrease(double numb) {
        if (numb >= 0) {
            balance -= numb;
        }
    }
}
