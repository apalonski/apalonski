package com.asoi.palonskiy.zd1;

import com.asoi.palonskiy.zd1.Card;

public class DebitCard extends Card {
    public DebitCard(String nam, double bal) {
        super(nam, bal);
    }

    @Override
    public void decrease(double numb) {
        if (numb < balance && numb >= 0) {
            balance -= numb;
        }
    }

}
