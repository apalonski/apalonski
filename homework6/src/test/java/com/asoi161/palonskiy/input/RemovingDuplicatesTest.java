package com.asoi161.palonskiy.input;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.Set;

import static org.junit.Assert.*;

public class RemovingDuplicatesTest {
    Set<String> set = new LinkedHashSet<>();
    ArrayList<String> splittedListOfLines = new ArrayList<>();
    @Before
    public void setLists() {

        set.add("a 3");
        set.add("b 2");
        set.add("bf 1");

        splittedListOfLines.add("a");
        splittedListOfLines.add("a");
        splittedListOfLines.add("a");
        splittedListOfLines.add("b");
        splittedListOfLines.add("b");
        splittedListOfLines.add("bf");
    }
    @Test
    public void removeDuplicates() {
        assertEquals(set,RemovingDuplicates.getSet(splittedListOfLines));
    }
}