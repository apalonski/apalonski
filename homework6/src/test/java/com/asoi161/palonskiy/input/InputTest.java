package com.asoi161.palonskiy.input;

import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.LinkedHashSet;
import java.util.Set;

import static org.junit.Assert.*;

public class InputTest {

    @Test
    public void parseInputTrue()throws Exception {
        Set<String> set = new LinkedHashSet<>();
        set.add("a 3");
        set.add("b 2");
        set.add("bf 1");

        String inputLine = "a b bf a b a";
        InputStream in = new ByteArrayInputStream(inputLine.getBytes());
        System.setIn(in);
        assertEquals(Input.getSet(), set);

    }

    @Test(expected = Exception.class)
    public void parseInputEmpty()throws Exception {
        String inputLine = " ";
        InputStream in = new ByteArrayInputStream(inputLine.getBytes());
        System.setIn(in);
        Input.getSet();
    }

    @Test(expected = Exception.class)
    public void parseInputTrash() throws Exception{
        String inputLine = ",..,./ ./,/ /.? /, /,. . , ";
        InputStream in = new ByteArrayInputStream(inputLine.getBytes());
        System.setIn(in);
        Input.getSet();
    }

    @Test
    public void validatingEmpty() {
        assertEquals(Input.validatingString(" "), null);
    }

    @Test
    public void validatingTrash() {
        assertEquals(Input.validatingString(",,,,  , ./. /? "), null);
    }

    @Test
    public void validatingTrue() {
        String input = "a b? vgd e, e jkl 1999";
        String finalInput = input.replaceAll("[-.?!)/(,:;'\"]", "");
        finalInput = finalInput.replaceAll("( +)", " ").trim();
        String[] splittedMasOfLines = finalInput.split(" ");
        assertArrayEquals(Input.validatingString(input), splittedMasOfLines);
    }
}