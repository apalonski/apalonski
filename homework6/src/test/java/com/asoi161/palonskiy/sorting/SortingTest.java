package com.asoi161.palonskiy.sorting;

import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.PrintStream;

import static org.junit.Assert.*;

public class SortingTest {

    private ByteArrayOutputStream output = new ByteArrayOutputStream();

    @Before
    public void setUpStreams() {
        System.setOut(new PrintStream(output));
    }

    @Test
    public void Sorting()throws Exception {
        String input = "a a a";
        InputStream in = new ByteArrayInputStream(input.getBytes());
        System.setIn(in);
        Sorting.getOutput();
        assertEquals("\n  A: a 3", output.toString());
    }
}