package com.asoi161.palonskiy.input;

import org.junit.Test;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.*;

public class RemovingDuplicatesTest {

    @Test
    public void removeDuplicates() {
        Set<String> set = new LinkedHashSet<>();
        set.add("a 3");
        set.add("b 2");
        set.add("bf 1");
        ArrayList<String> splittedListOfLines = new ArrayList<>();
        splittedListOfLines.add("a");
        splittedListOfLines.add("a");
        splittedListOfLines.add("a");
        splittedListOfLines.add("b");
        splittedListOfLines.add("b");
        splittedListOfLines.add("bf");
        assertEquals(set,RemovingDuplicates.removeDuplicates(splittedListOfLines));
    }
}