package com.asoi161.palonskiy.input;

import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.LinkedHashSet;
import java.util.Set;

import static org.junit.Assert.*;

public class InputTest {

    @Test
    public void parseInputTrue() {
        Set<String> set = new LinkedHashSet<>();
        set.add("a 3");
        set.add("b 2");
        set.add("bf 1");

        String input = "a b bf a b a";
        InputStream in = new ByteArrayInputStream(input.getBytes());
        System.setIn(in);

        assertEquals(Input.parseInput(), set);

    }

    @Test(expected = NullPointerException.class)
    public void parseInputEmpty() {
        String input = " ";
        InputStream in = new ByteArrayInputStream(input.getBytes());
        System.setIn(in);
        Input.parseInput();
    }

    @Test(expected = NullPointerException.class)
    public void parseInputTrash() {
        String input = ",..,./ ./,/ /.? /, /,. . , ";
        InputStream in = new ByteArrayInputStream(input.getBytes());
        System.setIn(in);
        Input.parseInput();
    }

    @Test
    public void validatingEmpty() {
        assertEquals(Input.validatingString(" "), null);
    }

    @Test
    public void validatingTrash() {
        assertEquals(Input.validatingString(",,,,  , ./. /? "), null);
    }

    @Test
    public void validatingTrue() {
        String input = "a b? vgd e, e jkl 1999";
        String finalInput = input.replaceAll("[-.?!)/(,:;'\"]", "");
        finalInput = finalInput.replaceAll("( +)", " ").trim();
        String[] splittedMasOfLines = finalInput.split(" ");
        assertArrayEquals(Input.validatingString(input), splittedMasOfLines);
    }

}