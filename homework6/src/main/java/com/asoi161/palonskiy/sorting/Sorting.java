package com.asoi161.palonskiy.sorting;

import com.asoi161.palonskiy.input.Input;



import java.util.ArrayList;


 public final class Sorting {
    private Sorting() throws Exception{

    }
    public static void getOutput() throws Exception{
        ArrayList<String> masOfLines = new ArrayList<>();
        masOfLines.addAll(Input.getSet());
        System.out.print("\n  "+masOfLines.get(0).substring(0, 1).toUpperCase() + ": " + masOfLines.get(0));
        for (int i = 1; i < masOfLines.size(); i++) {
            if (masOfLines.get(i).substring(0, 1).equalsIgnoreCase(masOfLines.get(i - 1).substring(0, 1))) {
                System.out.print("\n\t " + masOfLines.get(i).toLowerCase());
            } else {
                System.out.print("\n  " + masOfLines.get(i).substring(0, 1).toUpperCase() + ": " + masOfLines.get(i));
            }
        }
    }


}
