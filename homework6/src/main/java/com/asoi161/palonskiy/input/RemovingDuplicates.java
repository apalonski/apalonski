package com.asoi161.palonskiy.input;

import java.util.*;

 public final class RemovingDuplicates {
    public Set<String> set;
    private RemovingDuplicates (){
    }
    public static Set<String> getSet(List<String> splittedListOfLines){
        int occurrences=0;
        Set<String> set = new LinkedHashSet<>();
        for (int i = 0 ; i < splittedListOfLines.size() ; i++) {
            occurrences = Collections.frequency(splittedListOfLines, splittedListOfLines.get(i));
            set.add(splittedListOfLines.get(i)+" "+occurrences);
            Set<String> finalSet= set;
        }
        return set;
    }
}
