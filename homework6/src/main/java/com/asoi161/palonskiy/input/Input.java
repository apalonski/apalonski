package com.asoi161.palonskiy.input;


import java.util.*;

 public final class Input {
    public Set<String> set;
    private Input ()throws Exception{

    }
    public static Set<String>  getSet () throws Exception{
        Scanner scanner = new Scanner(System.in);
        String inputString = scanner.nextLine();
        String[] splittedMasOfLines = validatingString(inputString);
        if (splittedMasOfLines != null) {
            for (int i = 0; i < splittedMasOfLines.length; i++) {
                splittedMasOfLines[i] = splittedMasOfLines[i].toLowerCase();
            }
            Arrays.sort(splittedMasOfLines);
            ArrayList<String> splittedListOfLines = new ArrayList<>();
            splittedListOfLines.addAll(Arrays.asList(splittedMasOfLines));
            return RemovingDuplicates.getSet(splittedListOfLines);
        } else {
            throw new Exception();
        }
    }

    public static String[] validatingString(String inputString) {
        String[] splittedMasOfLines = null;
        String test = inputString.replaceAll("\\W", "");
        if (!test.equals("")) {
            String finalInput = inputString.replaceAll("[-.?!)/(,:;'\"]", "");
            finalInput = finalInput.replaceAll("( +)", " ").trim();

            return finalInput.split(" ");
        } else {
            return splittedMasOfLines;
        }
    }

}
